﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BrightSpark.Validator
{
    public class PropertyConfigurator<T, TProp> : PropertyConfigurator<IValidator<T>, T, TProp, PropertyConfigurator<T, TProp>>
    {
        public PropertyConfigurator(IValidator<T> validator, Expression<Func<T, TProp>> ex) : base(validator, ex)
        {
        }
    }

    public class PropertyConfigurator<TValidator, T, TProp, TRes>
        where TValidator : IValidator<T>
        where TRes : PropertyConfigurator<TValidator, T, TProp, TRes>
    {
        protected readonly TValidator Validator;
        protected readonly Expression<Func<T, TProp>> Ex;
        public readonly Func<T, TProp> GetValue;

        protected string MemberName => ValidatorUtil.GetMemberName(Ex);

        public PropertyConfigurator(TValidator validator, Expression<Func<T, TProp>> ex)
        {
            Validator = validator;
            Ex = ex;
            GetValue = ex.MakeSafeGet();
        }

        public virtual TRes TrueByValue(Func<TProp, bool> customValidator, string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} must be true";
            Validator.IsTrueByValue(MemberName, GetValue, customValidator, err, errorCode);
            return this as TRes;
        }

        public TRes Required(string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} is required";
            Validator.Required(MemberName, Ex, err, errorCode);
            return this as TRes;
        }

        public TRes ValidBy(Func<T, bool> customValidator, string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} is not valid";
            Validator.IsTrue(MemberName, customValidator, err, errorCode);
            return this as TRes;
        }

        public TRes ValidBy(Func<T, string, bool> customValidator, string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} is not valid";
            Validator.IsTrue(MemberName, customValidator, err, errorCode);
            return this as TRes;
        }

        public TRes ValidBy(Func<T, ValidationResult> validator)
        {
            Validator.IsTrue(MemberName, validator);
            return this as TRes;
        }

        public TRes ValidBy(Func<T, string, ValidationResult> validator)
        {
            Validator.IsTrue(MemberName, validator);
            return this as TRes;
        }

        public TRes ValidBy(Func<T, IEnumerable<ValidationResult>> validator)
        {
            Validator.IsTrue(MemberName, validator);
            return this as TRes;
        }

        public TRes ValidBy(Func<T, string, IEnumerable<ValidationResult>> validator)
        {
            Validator.IsTrue(MemberName, validator);
            return this as TRes;
        }

        public TRes ValidByAsync(Func<T, Task<bool>> customValidator, string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} is not valid";
            Validator.IsTrueAsync(MemberName, (x, k) => customValidator(x), err, errorCode);
            return this as TRes;
        }

        public TRes ValidByAsync(Func<T, Task<ValidationResult>> validator)
        {
            Validator.IsTrueAsync(MemberName, validator);
            return this as TRes;
        }

        public TRes ValidByAsync(Func<T, Task<IEnumerable<ValidationResult>>> validator)
        {
            Validator.IsTrueAsync(MemberName, validator);
            return this as TRes;
        }

        public TRes ValidByAsync(Func<T, string, Task<ValidationResult>> validator)
        {
            Validator.IsTrueAsync(MemberName, validator);
            return this as TRes;
        }

        public TRes ValidByAsync(Func<T, string, Task<IEnumerable<ValidationResult>>> validator)
        {
            Validator.IsTrueAsync(MemberName, validator);
            return this as TRes;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="TProp2"></typeparam>
        /// <param name="value"></param>
        /// <param name="errorMessage">
        /// arguments:
        /// {0} - source value i.e model property value
        /// {1} - other value i.e. given value
        /// </param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        public TRes GreaterThan<TProp2>(TProp2 value, string errorMessage = null, int? errorCode = null) where TProp2 : TProp, IComparable<TProp2>
        {
            var err = errorMessage ?? $"{MemberName} must be greater than {{1}}";
            Validator.IsGreaterThan(MemberName, x => (TProp2)GetValue(x), value, err, errorCode);
            return this as TRes;
        }

        public TRes GreaterThan<TProp2>(Func<T, TProp2> value, string errorMessage = null, int? errorCode = null) where TProp2 : TProp, IComparable<TProp2>
        {
            var err = errorMessage ?? $"{MemberName} must be greater than {{1}}";
            Validator.IsGreaterThan(MemberName, x => (TProp2)GetValue(x), value, err, errorCode);
            return this as TRes;
        }

        public TRes GreaterThanOrEqualTo<TProp2>(TProp2 value, string errorMessage = null, int? errorCode = null)
            where TProp2 : TProp, IComparable<TProp2>
        {
            var err = errorMessage ?? $"{MemberName} must be greater than or equal to {{1}}";
            Validator.IsGreaterThanOrEqualTo(MemberName, x => (TProp2)GetValue(x), value, err, errorCode);
            return this as TRes;
        }

        public TRes GreaterThanOrEqualTo<TProp2>(Func<T, TProp2> value, string errorMessage = null, int? errorCode = null)
            where TProp2 : TProp, IComparable<TProp2>
        {
            var err = errorMessage ?? $"{MemberName} must be greater than or equal to {{1}}";
            Validator.IsGreaterThanOrEqualTo(MemberName, x => (TProp2)GetValue(x), value, err, errorCode);
            return this as TRes;
        }

        public TRes LessThan<TProp2>(TProp2 value, string errorMessage = null, int? errorCode = null) where TProp2 : TProp, IComparable<TProp2>
        {
            var err = errorMessage ?? $"{MemberName} must be less than {{1}}";
            Validator.IsLessThan(MemberName, x => (TProp2)GetValue(x), value, err, errorCode);
            return this as TRes;
        }

        public TRes LessThan<TProp2>(Func<T, TProp2> value, string errorMessage = null, int? errorCode = null) where TProp2 : TProp, IComparable<TProp2>
        {
            var err = errorMessage ?? $"{MemberName} must be less than {{1}}";
            Validator.IsLessThan(MemberName, x => (TProp2)GetValue(x), value, err, errorCode);
            return this as TRes;
        }

        public TRes LessThanOrEqualTo<TProp2>(TProp2 value, string errorMessage = null, int? errorCode = null)
            where TProp2 : TProp, IComparable<TProp2>
        {
            var err = errorMessage ?? $"{MemberName} must be less than or equal to {{1}}";
            Validator.IsLessThanOrEqualTo(MemberName, x => (TProp2)GetValue(x), value, err, errorCode);
            return this as TRes;
        }

        public TRes LessThanOrEqualTo<TProp2>(Func<T, TProp2> value, string errorMessage = null, int? errorCode = null)
            where TProp2 : TProp, IComparable<TProp2>
        {
            var err = errorMessage ?? $"{MemberName} must be less than or equal to {{1}}";
            Validator.IsLessThanOrEqualTo(MemberName, x => (TProp2)GetValue(x), value, err, errorCode);
            return this as TRes;
        }

        public TRes EqualTo<TProp2>(TProp2 value, string errorMessage = null, int? errorCode = null)
            where TProp2 : TProp, IComparable<TProp2>
        {
            var err = errorMessage ?? $"{MemberName} must be equal to {{1}}";
            Validator.IsEqualTo(MemberName, x => (TProp2)GetValue(x), value, err, errorCode);
            return this as TRes;
        }

        public TRes EqualTo<TProp2>(Func<T, TProp2> value, string errorMessage = null, int? errorCode = null)
            where TProp2 : TProp, IComparable<TProp2>
        {
            var err = errorMessage ?? $"{MemberName} must be equal to {{1}}";
            Validator.IsEqualTo(MemberName, x => (TProp2)GetValue(x), value, err, errorCode);
            return this as TRes;
        }
        
        public TRes ValidObject(IValidator<TProp> validator, string errorMessage = null, int? errorCode = null)
        {
            Validator.ValidateObject(Ex, validator, errorMessage, errorCode);
            return this as TRes;
        }
    }
}