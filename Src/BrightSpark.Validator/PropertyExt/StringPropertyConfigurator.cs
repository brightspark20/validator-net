﻿using System;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace BrightSpark.Validator
{
    public class StringPropertyConfigurator<T> : StringPropertyConfigurator<IValidator<T>, T, StringPropertyConfigurator<T>>
    {
        public StringPropertyConfigurator(IValidator<T> validator, Expression<Func<T, string>> ex) : base(validator, ex)
        {
        }
    }

    public class StringPropertyConfigurator<TValidator, T, TRes> : PropertyConfigurator<TValidator, T, string, StringPropertyConfigurator<TValidator, T, TRes>>
        where TValidator : IValidator<T>
        where TRes : StringPropertyConfigurator<TValidator, T, TRes>
    {
        public StringPropertyConfigurator(TValidator validator, Expression<Func<T, string>> ex) : base(validator, ex)
        {
        }

        public TRes NotNullOrEmpty(string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} can not be null or empty";
            Validator.IsNotNullOrEmpty(MemberName, GetValue, err, errorCode);
            return this as TRes;
        }

        public TRes NotNullOrWhiteSpace(string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} can not be null or whitespace";
            Validator.IsNotNullOrEmpty(MemberName, x => GetValue(x)?.Trim(), err, errorCode);
            return this as TRes;
        }

        public TRes Null(string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} must be null";
            Validator.IsNull(MemberName, GetValue, err, errorCode);
            return this as TRes;
        }

        public TRes Numeric(string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} must be numeric";
            Validator.IsNumeric(MemberName, GetValue, err, errorCode);
            return this as TRes;
        }

        public TRes Integer(string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} must be integer";
            Validator.IsInteger(MemberName, GetValue, err, errorCode);
            return this as TRes;
        }

        public TRes ValidEstonianIdentityCode(string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} is not valid Estonian ID code";
            Validator.IsValidEstonianIdentityCode(MemberName, GetValue, err, errorCode);
            return this as TRes;
        }

        public TRes Email(string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} is not valid e-mail address";
            Validator.IsEmail(MemberName, GetValue, err, errorCode);
            return this as TRes;
        }

        public TRes Guid(string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} is not valid GUID";
            Validator.IsGuid(MemberName, GetValue, err, errorCode);
            return this as TRes;
        }

        public TRes Date(string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} is not valid date";
            Validator.IsDate(MemberName, GetValue, err, errorCode);
            return this as TRes;
        }

        public TRes Uri(string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} is not valid uri";
            Validator.IsUri(MemberName, GetValue, err, errorCode);
            return this as TRes;
        }

        public TRes WithMaxLength(int maxLength, string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} exceeded max length ({maxLength})";
            Validator.MaxLength(MemberName, GetValue, maxLength, err, errorCode);
            return this as TRes;
        }

        public TRes WithMinLength(int minLength, string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} must be at least {minLength} characters long";
            Validator.MinLength(MemberName, GetValue, minLength, err, errorCode);
            return this as TRes;
        }

        public TRes WithRegex(string pattern, string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} does not match pattern {pattern}";
            Validator.Regex(MemberName, GetValue, pattern, err, errorCode);
            return this as TRes;
        }

        public TRes WithRegex(Regex regex, string errorMessage = null, int? errorCode = null)
        {
            var err = errorMessage ?? $"{MemberName} does not match pattern {regex}";
            Validator.Regex(MemberName, GetValue, regex, err, errorCode);
            return this as TRes;
        }
    }
}