﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BrightSpark.Validator
{
    public class CollectionPropertyConfigurator<T, TProp> : CollectionPropertyConfigurator<IValidator<T>, T, TProp, CollectionPropertyConfigurator<T, TProp>>
    {
        public CollectionPropertyConfigurator(IValidator<T> validator, Expression<Func<T, IEnumerable<TProp>>> ex) : base(validator, ex)
        {
        }
    }

    public class CollectionPropertyConfigurator<TValidator, T, TProp, TRes>
        where TValidator : IValidator<T>
        where TRes : CollectionPropertyConfigurator<TValidator, T, TProp, TRes>
    {
        protected readonly TValidator Validator;
        protected readonly Expression<Func<T, IEnumerable<TProp>>> Ex;
        public readonly Func<T, IEnumerable<TProp>> GetValue;

        protected string MemberName => ValidatorUtil.GetMemberName(Ex);

        public CollectionPropertyConfigurator(TValidator validator, Expression<Func<T, IEnumerable<TProp>>> ex)
        {
            Validator = validator;
            Ex = ex;
            GetValue = ex.MakeSafeGet();
        }

        public TRes Required(string errorMessage = null, int? errorCode = null)
        {
            Validator.Required(Ex, errorMessage, errorCode);
            return this as TRes;
        }

        public TRes ValidCollection(IValidator<TProp> validator, string errorMessage = null, int? errorCode = null)
        {
            Validator.ValidateCollection(Ex, validator, errorMessage, errorCode);
            return this as TRes;
        }
    }
}