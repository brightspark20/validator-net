﻿using System;
using System.Linq.Expressions;

namespace BrightSpark.Validator
{
    public class PropertyValidatorDescriptor<T, TProp>
    {
        private readonly IValidator<T> _validator;
        private readonly Expression<Func<T, TProp>> _ex;

        internal PropertyValidatorDescriptor(IValidator<T> validator, Expression<Func<T, TProp>> ex)
        {
            _validator = validator;
            _ex = ex;
        }

        public IValidator<T> Is(Action<PropertyConfigurator<T, TProp>> config)
        {
            config(new PropertyConfigurator<T, TProp>(_validator, _ex));
            return _validator;
        }
    }
}