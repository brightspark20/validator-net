﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BrightSpark.Validator
{
    public class CollectionPropertyValidatorDescriptor<T, TProp>
    {
        private readonly IValidator<T> _validator;
        private readonly Expression<Func<T, IEnumerable<TProp>>> _ex;

        internal CollectionPropertyValidatorDescriptor(IValidator<T> validator, Expression<Func<T, IEnumerable<TProp>>> ex)
        {
            _validator = validator;
            _ex = ex;
        }

        public IValidator<T> Is(Action<CollectionPropertyConfigurator<T, TProp>> config)
        {
            config(new CollectionPropertyConfigurator<T, TProp>(_validator, _ex));
            return _validator;
        }
    }
}