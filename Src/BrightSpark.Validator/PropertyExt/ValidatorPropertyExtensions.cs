﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace BrightSpark.Validator
{
    public static class ValidatorPropertyExtensions
    {
        public static PropertyValidatorDescriptor<T, TProp> Property<T, TProp>(this IValidator<T> validator, Expression<Func<T, TProp>> ex)
        {
            return new PropertyValidatorDescriptor<T, TProp>(validator, ex);
        }

        public static StringPropertyValidatorDescriptor<T> Property<T>(this IValidator<T> validator, Expression<Func<T, string>> ex)
        {
            return new StringPropertyValidatorDescriptor<T>(validator, ex);
        }

        public static CollectionPropertyValidatorDescriptor<T, TProp> Collection<T, TProp>(this IValidator<T> validator, Expression<Func<T, IEnumerable<TProp>>> ex)
        {
            return new CollectionPropertyValidatorDescriptor<T, TProp>(validator, ex);
        }
    }
}
