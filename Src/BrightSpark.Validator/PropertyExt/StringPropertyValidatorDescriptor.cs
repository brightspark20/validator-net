﻿using System;
using System.Linq.Expressions;

namespace BrightSpark.Validator
{
    public class StringPropertyValidatorDescriptor<T>
    {
        private readonly IValidator<T> _validator;
        private readonly Expression<Func<T, string>> _ex;

        internal StringPropertyValidatorDescriptor(IValidator<T> validator, Expression<Func<T, string>> ex)
        {
            _validator = validator;
            _ex = ex;
        }

        public IValidator<T> Is(Action<StringPropertyConfigurator<T>> config)
        {
            config(new StringPropertyConfigurator<T>(_validator, _ex));
            return _validator;
        }
    }
}