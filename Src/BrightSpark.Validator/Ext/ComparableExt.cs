﻿using System;
using System.Linq.Expressions;

namespace BrightSpark.Validator
{
    public static class ComparableExt
    {
        /// <summary>
        /// Add validator which checks if model property value is greater than given item
        /// </summary>
        [ValidatorId("857B3DA7-BC6B-4876-A434-0B62045BBA6F")]
        public static IValidator<T> IsGreaterThan<T, TProp>(
            this IValidator<T> validator, 
            Expression<Func<T, TProp>> predicate,
            TProp value, 
            string errorMessage = "*", 
            int? errorCode = null) where TProp : IComparable<TProp>
        {
            var get = predicate.MakeSafeGet();
            var key = ValidatorUtil.GetMemberName(predicate);
            return validator.IsGreaterThan(key, get, value, errorMessage, errorCode);
        }

        public static IValidator<T> IsGreaterThan<T, TProp>(
            this IValidator<T> validator, 
            string key,
            Func<T, TProp> get, 
            TProp value, 
            string errorMessage = "*", 
            int? errorCode = null) where TProp : IComparable<TProp>
        {
            return validator.IsGreaterThan(key, get, x => value, errorMessage, errorCode);
        }

        public static IValidator<T> IsGreaterThan<T, TProp>(
            this IValidator<T> validator, 
            string key,
            Func<T, TProp> get, 
            Func<T, TProp> value, 
            string errorMessage = "*", 
            int? errorCode = null) where TProp : IComparable<TProp>
        {
            return validator.Compare(key, get, value, x => x > 0, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property value is greater than or equal to given item
        /// </summary>
        [ValidatorId("739D1376-6771-457A-B8FA-76A2D847AC47")]
        public static IValidator<T> IsGreaterThanOrEqualTo<T, TProp>(
            this IValidator<T> validator, 
            Expression<Func<T, TProp>> predicate,
            TProp value, 
            string errorMessage = "*", 
            int? errorCode = null) where TProp : IComparable<TProp>
        {
            var get = predicate.MakeSafeGet();
            var key = ValidatorUtil.GetMemberName(predicate);
            return validator.IsGreaterThanOrEqualTo(key, get, value, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property value is greater than or equal to given item
        /// </summary>
        [ValidatorId("C6DB614C-0FE9-4625-80B4-EF3F0FD6E1A5")]
        public static IValidator<T> IsGreaterThanOrEqualTo<T, TProp>(
            this IValidator<T> validator, 
            string key, 
            Func<T, TProp> get,
            TProp value, 
            string errorMessage = "*", 
            int? errorCode = null) where TProp : IComparable<TProp>
        {
            return validator.IsGreaterThanOrEqualTo(key, get, x => value, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property value is greater than or equal to given item
        /// </summary>
        [ValidatorId("C6DB614C-0FE9-4625-80B4-EF3F0FD6E1A5")]
        public static IValidator<T> IsGreaterThanOrEqualTo<T, TProp>(
            this IValidator<T> validator, 
            string key, 
            Func<T, TProp> get,
            Func<T, TProp> value, 
            string errorMessage = "*", 
            int? errorCode = null) where TProp : IComparable<TProp>
        {
            return validator.Compare(key, get, value, x => x >= 0, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property value is less than given item
        /// </summary>
        [ValidatorId("B205C931-E405-4EE3-8418-D5199BBD82B1")]
        public static IValidator<T> IsLessThan<T, TProp>(
            this IValidator<T> validator, 
            Expression<Func<T, TProp>> predicate,
            TProp value, 
            string errorMessage = "*", 
            int? errorCode = null) where TProp : IComparable<TProp>
        {
            return validator.IsLessThan(predicate, x => value, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property value is less than given item
        /// </summary>
        [ValidatorId("B205C931-E405-4EE3-8418-D5199BBD82B1")]
        public static IValidator<T> IsLessThan<T, TProp>(
            this IValidator<T> validator,
            Expression<Func<T, TProp>> predicate,
            Func<T, TProp> value,
            string errorMessage = "*",
            int? errorCode = null) where TProp : IComparable<TProp>
        {
            var get = predicate.MakeSafeGet();
            var key = ValidatorUtil.GetMemberName(predicate);
            return validator.IsLessThan(key, get, value, errorMessage, errorCode);
        }

        public static IValidator<T> IsLessThan<T, TProp>(
            this IValidator<T> validator,
            string key,
            Func<T, TProp> get,
            TProp value,
            string errorMessage = "*",
            int? errorCode = null) where TProp : IComparable<TProp>
        {
            return validator.IsLessThan(key, get, x => value, errorMessage, errorCode);
        }

        public static IValidator<T> IsLessThan<T, TProp>(
            this IValidator<T> validator,
            string key,
            Func<T, TProp> get,
            Func<T, TProp> value,
            string errorMessage = "*",
            int? errorCode = null) where TProp : IComparable<TProp>
        {
            return validator.Compare(key, get, value, x => x < 0, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property value is less than or equal to given item
        /// </summary>
        [ValidatorId("030C4056-5CF7-431C-B1DE-0C26B2A14EB2")]
        public static IValidator<T> IsLessThanOrEqualTo<T, TProp>(
            this IValidator<T> validator, 
            Expression<Func<T, TProp>> predicate,
            TProp item, 
            string errorMessage = "*", 
            int? errorCode = null) where TProp : IComparable<TProp>
        {
            var get = predicate.MakeSafeGet();
            var key = ValidatorUtil.GetMemberName(predicate);
            return validator.IsLessThanOrEqualTo(key, get, x => item, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property value is less than or equal to given item
        /// </summary>
        [ValidatorId("030C4056-5CF7-431C-B1DE-0C26B2A14EB2")]
        public static IValidator<T> IsLessThanOrEqualTo<T, TProp>(
            this IValidator<T> validator,
            string key,
            Func<T, TProp> get,
            TProp value,
            string errorMessage = "*",
            int? errorCode = null) where TProp : IComparable<TProp>
        {
            return validator.IsLessThanOrEqualTo(key, get, x => value, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property value is less than or equal to given item
        /// </summary>
        [ValidatorId("030C4056-5CF7-431C-B1DE-0C26B2A14EB2")]
        public static IValidator<T> IsLessThanOrEqualTo<T, TProp>(
            this IValidator<T> validator, 
            string key,
            Func<T, TProp> get,
            Func<T, TProp> value, 
            string errorMessage = "*", 
            int? errorCode = null) where TProp : IComparable<TProp>
        {
            return validator.Compare(key, get, value, x => x <= 0, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property value is equal to given item
        /// </summary>
        [ValidatorId("B15290B6-D500-4AFE-AD07-C0040686F0F4")]
        public static IValidator<T> IsEqualTo<T, TProp>(
            this IValidator<T> validator, 
            Expression<Func<T, TProp>> predicate,
            TProp value, 
            string errorMessage = "*", 
            int? errorCode = null) where TProp : IComparable<TProp>
        {
            var get = predicate.MakeSafeGet();
            var key = ValidatorUtil.GetMemberName(predicate);
            return validator.IsEqualTo(key, get, x => value, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property value is equal to given item
        /// </summary>
        [ValidatorId("B15290B6-D500-4AFE-AD07-C0040686F0F4")]
        public static IValidator<T> IsEqualTo<T, TProp>(
            this IValidator<T> validator,
            string key,
            Func<T, TProp> get,
            TProp value,
            string errorMessage = "*",
            int? errorCode = null) where TProp : IComparable<TProp>
        {
            return validator.IsEqualTo(key, get, x => value, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property value is equal to given item
        /// </summary>
        [ValidatorId("B15290B6-D500-4AFE-AD07-C0040686F0F4")]
        public static IValidator<T> IsEqualTo<T, TProp>(
            this IValidator<T> validator, 
            string key,
            Func<T, TProp> get,
            Func<T, TProp> value, 
            string errorMessage = "*", 
            int? errorCode = null) where TProp : IComparable<TProp>
        {
            return validator.Compare(key, get, value, x => x == 0, errorMessage, errorCode);
        }

        private static IValidator<T> Compare<T, TProp>(
            this IValidator<T> validator,
            string key,
            Func<T, TProp> get,
            Func<T, TProp> value,
            Func<int, bool> isCompareResultValid,
            string errorMessage,
            int? errorCode) where TProp : IComparable<TProp>
        {
            return validator.IsTrue(key, x =>
            {
                var source = get(x);
                var other = value(x);

                var compareResult = source.CompareTo(other);
                if (isCompareResultValid(compareResult))
                {
                    return ValidationResult.Ok(key);
                }

                return ValidationResult.Ok(key)
                    .AddError(new ValidationError(errorMessage, errorCode, null, source, other));
            });
        }
    }
}
