﻿using System;
using System.Globalization;
using System.Linq.Expressions;
using System.Text.RegularExpressions;

namespace BrightSpark.Validator
{
    public static class StringExt
    {
        /// <summary>
        /// Add validator which checks if model string property value is not null or empty
        /// </summary>
        [ValidatorId("38CE0120-DA95-43B7-88AB-84C4AE14D09B")]
        public static IValidator<T> IsNotNullOrEmpty<T>(this IValidator<T> validator,
            Expression<Func<T, string>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            var key = ValidatorUtil.GetMemberName(predicate);
            var get = predicate.MakeSafeGet();

            return validator.IsNotNullOrEmpty(key, get, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property value is not null or empty.
        /// Key can be different from model property selector.
        /// </summary>
        [ValidatorId("67A151CE-E762-4B7B-9F0B-18FC3D5EE77B")]
        public static IValidator<T> IsNotNullOrEmpty<T>(this IValidator<T> validator, string key, Func<T, string> get, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                var value = get(x);
                return value != null && value.Trim() != string.Empty;
            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if string property value is not null or empty
        /// </summary>
        [ValidatorId("BBC1BC12-4D24-482F-986F-CF3D4C4D44EF")]
        public static IValidator<T> IsNotNullOrEmpty<T>(this IValidator<T> validator,
            string key, string value, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(key, x => value != null && value.Trim() != string.Empty, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property value is null
        /// </summary>
        [ValidatorId("9A3E5E34-3B0D-4D41-942F-336320EC0BBE")]
        public static IValidator<T> IsNull<T>(this IValidator<T> validator, Expression<Func<T, string>> predicate,
            string errorMessage = "*", int? errorCode = null)
        {
            var key = ValidatorUtil.GetMemberName(predicate);
            var get = predicate.MakeSafeGet();

            return validator.IsNull(key, get, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property value is null
        /// </summary>
        [ValidatorId("6766E4BB-4AC8-4D30-8567-25562DB3C935")]
        public static IValidator<T> IsNull<T>(this IValidator<T> validator, string key, Func<T, string> get, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                var value = get(x);
                return value == null;
            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property length is less than or equal to given min length.
        /// </summary>
        /// <returns></returns>
        [ValidatorId("3BA2E30A-2E22-4298-B187-1425B4C76774")]
        public static IValidator<T> MinLength<T>(this IValidator<T> validator, Expression<Func<T, string>> predicate, int minLength, string errorMessage = "*", int? errorCode = null)
        {
            var info = PredicateInfo.From(predicate);
            return validator.MinLength(info.MemberName, info.GetValue, minLength, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property length is less than or equal to given min length.
        /// </summary>
        /// <returns></returns>
        [ValidatorId("50DD9CEF-07E6-4E26-AD36-BAE587992C04")]
        public static IValidator<T> MinLength<T>(this IValidator<T> validator, string key, Func<T, string> get, int minLength, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                var value = get(x);
                return string.IsNullOrEmpty(value) || value.Length >= minLength;
            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property length is greater than or equal to given length
        /// </summary>
        /// <returns></returns>
        [ValidatorId("4EE7994E-791E-48F4-B9D1-01884AB8DE47")]
        public static IValidator<T> MaxLength<T>(this IValidator<T> validator, Expression<Func<T, string>> predicate, int maxLength, string errorMessage = "*", int? errorCode = null)
        {
            var info = PredicateInfo.From(predicate);
            return validator.MaxLength(info.MemberName, info.GetValue, maxLength, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property length is greater than or equal to given length
        /// </summary>
        /// <returns></returns>
        [ValidatorId("464C8246-99D8-47AF-ABA3-4EE728FDF38A")]
        public static IValidator<T> MaxLength<T>(this IValidator<T> validator, string key, Func<T, string> get, int maxLength, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                var value = get(x);
                return string.IsNullOrEmpty(value) || value.Length <= maxLength;
            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid e-mail address
        /// </summary>
        /// <returns></returns>
        [ValidatorId("B437547C-23BF-4262-9D8C-8D70E2499A95")]
        public static IValidator<T> IsEmail<T>(this IValidator<T> validator, Expression<Func<T, string>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            var info = PredicateInfo.From(predicate);
            return validator.IsEmail(info.MemberName, info.GetValue, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid e-mail address
        /// </summary>
        [ValidatorId("22B4C9C2-9967-488A-AFCF-0DF788CBCA2F")]
        public static IValidator<T> IsEmail<T>(this IValidator<T> validator, string key, Func<T, string> get, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                var value = get(x);

                if (string.IsNullOrEmpty(value))
                {
                    return true;
                }

                const string strRegex = @"^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$";
                const RegexOptions options = RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture;

                var regex = new Regex(strRegex, options);
                return regex.IsMatch(value);

            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid uri
        /// </summary>
        [ValidatorId("AA1C6D69-9E4F-44E1-9F55-3907BBB03328")]
        public static IValidator<T> IsUri<T>(this IValidator<T> validator, Expression<Func<T, string>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            var get = predicate.MakeSafeGet();

            return validator.IsUri(memberName, get, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid uri
        /// </summary>
        [ValidatorId("96803155-7777-4907-AC31-0E4600E3B164")]
        public static IValidator<T> IsUri<T>(this IValidator<T> validator, string key, Func<T, string> get, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                var value = get(x);

                if (string.IsNullOrEmpty(value))
                {
                    return true;
                }

                try
                {
                    new Uri(value);
                    return true;
                }
                catch
                {
                    return false;
                }

            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid date
        /// </summary>
        [ValidatorId("71B656B4-9267-4C4A-99B5-4D66DE6AEAA9")]
        public static IValidator<T> IsDate<T>(this IValidator<T> validator, Expression<Func<T, string>> predicate, string errorMessage, int? errorCode = null)
        {
            string key = ValidatorUtil.GetMemberName(predicate);
            var get = predicate.MakeSafeGet();

            return validator.IsDate(key, get, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid date		
        /// </summary>
        [ValidatorId("D55EE152-C227-4442-814E-D3BD201EE32A")]
        public static IValidator<T> IsDate<T>(this IValidator<T> validator, string key, Func<T, string> get, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                var value = get(x);
                return string.IsNullOrEmpty(value) || DateTime.TryParse(value, out _);

            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid integer
        /// </summary>
        [ValidatorId("51306DBC-77F6-4074-B41D-1B213720B020")]
        public static IValidator<T> IsInteger<T>(this IValidator<T> validator, Expression<Func<T, string>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            string key = ValidatorUtil.GetMemberName(predicate);
            var get = predicate.MakeSafeGet();

            return validator.IsInteger(key, get, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid integer
        /// </summary>
        [ValidatorId("687D06F2-1822-48E4-9C7A-B240CFB58B5A")]
        public static IValidator<T> IsInteger<T>(this IValidator<T> validator, string key, Func<T, string> get, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                var value = get(x);
                return string.IsNullOrEmpty(value) || int.TryParse(value, out _);

            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid numeric value
        /// </summary>
        [ValidatorId("8503DAC7-57A0-4F6C-9FA1-833E9C6EF343")]
        public static IValidator<T> IsNumeric<T>(this IValidator<T> validator, Expression<Func<T, string>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            string key = ValidatorUtil.GetMemberName(predicate);
            var get = predicate.MakeSafeGet();

            return validator.IsNumeric(key, get, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property is valid numeric value
        /// </summary>
        [ValidatorId("3773B497-B6EC-4953-832A-20B519683549")]
        public static IValidator<T> IsNumeric<T>(this IValidator<T> validator, string key, Func<T, string> get, string errorMessage, int? errorCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                var value = get(x);
                return string.IsNullOrEmpty(value) 
                       || decimal.TryParse(value.Replace(",", "."), NumberStyles.Any, CultureInfo.InvariantCulture, out _);
            }, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property matches given regex pattern
        /// </summary>
        [ValidatorId("BAE9D9AE-8E6C-41BC-8952-77F4AA776592")]
        public static IValidator<T> Regex<T>(this IValidator<T> validator, Expression<Func<T, string>> predicate, string pattern, string errorMessage = "*", int? errorCode = null)
        {
            var info = PredicateInfo.From(predicate);
            return validator.Regex(info.MemberName, info.GetValue, pattern, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property matches given regex pattern
        /// </summary>
        [ValidatorId("83452972-12D2-43EF-9973-B3D226540D8B")]
        public static IValidator<T> Regex<T>(this IValidator<T> validator, string key, Func<T, string> get, string pattern, string errorMessage = "*", int? errCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                var input = get(x);
                return string.IsNullOrEmpty(input) || System.Text.RegularExpressions.Regex.IsMatch(input, pattern);
            }, errorMessage, errCode);
        }

        /// <summary>
        /// Add validator which checks if model string property matches given regex pattern
        /// </summary>
        [ValidatorId("541BE591-3ED6-4EBD-AC21-22E14BB906FD")]
        public static IValidator<T> Regex<T>(this IValidator<T> validator, Expression<Func<T, string>> predicate, Regex regex, string errorMessage = "*", int? errorCode = null)
        {
            var info = PredicateInfo.From(predicate);
            return validator.Regex(info.MemberName, info.GetValue, regex, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model string property matches given regex pattern
        /// </summary>
        [ValidatorId("83452972-12D2-43EF-9973-B3D226540D8B")]
        public static IValidator<T> Regex<T>(this IValidator<T> validator, string key, Func<T, string> get, Regex regex, string errorMessage = "*", int? errCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                var input = get(x);
                return string.IsNullOrEmpty(input) || regex.IsMatch(input);
            }, errorMessage, errCode);
        }

        ///// <summary>
        ///// Add validator which checks if model string property matches given regex pattern or is null
        ///// </summary>
        //[ValidatorId("905E9433-435A-4BEA-B33F-BE68CB65C596")]
        //public static IValidator<T> RegexOrNull<T>(this IValidator<T> validator, Expression<Func<T, string>> predicate, string pattern, string errorMessage = "*", int? errCode = null)
        //{
        //    var get = predicate.MakeSafeGet();

        //    return validator.IsTrue(predicate, x =>
        //    {
        //        string input = get(x);
        //        return input == null || System.Text.RegularExpressions.Regex.IsMatch(input, pattern);
        //    }, errorMessage, errCode);
        //}

        /// <summary>
        /// Add validator that checks if model string property is in guid format
        /// </summary>
        [ValidatorId("715AC75B-C51B-442B-9B45-99D6C2AFF586")]
        public static IValidator<T> IsGuid<T>(this IValidator<T> validator, Expression<Func<T, string>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            var info = PredicateInfo.From(predicate);
            return validator.IsGuid(info.MemberName, info.GetValue, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator that checks if model string property is in guid format.
        /// </summary>
        [ValidatorId("08E3BD5E-25E7-4244-BB6E-F3928FFF187E")]
        public static IValidator<T> IsGuid<T>(this IValidator<T> validator, string key, Func<T, string> get, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                var value = get(x);
                return string.IsNullOrEmpty(value) || Guid.TryParse(value, out _);
            });
        }

        ///// <summary>
        ///// Add validator that checks if model string property is in guid format or is null.
        ///// </summary>
        //[ValidatorId("16B1D58B-9C98-483E-BA1E-801C2565E410")]
        //public static IValidator<T> IsGuidOrNull<T>(this IValidator<T> validator, Expression<Func<T, string>> predicate, string errorMessage = "*", int? errorCode = null)
        //{
        //    return IsGuidOrNull(validator, ValidatorUtil.GetMemberName(predicate), predicate, errorMessage, errorCode);
        //}

        /// <summary>
        /// Add validator that checks if model string property is in guid format or is null.
        /// </summary>
        //[ValidatorId("AF9A32CC-9F96-440C-85C1-4DE52EF954F1")]
        //public static IValidator<T> IsGuidOrNull<T>(this IValidator<T> validator, string key, Expression<Func<T, string>> predicate, string errorMessage = "*", int? errorCode = null)
        //{
        //    var get = predicate.MakeSafeGet();

        //    return validator.IsTrue(key, x =>
        //    {
        //        var value = get(x);
        //        return value == null || Guid.TryParse(value, out _);
        //    });
        //}

        private class PredicateInfo<T>
        {
            public Func<T, string> GetValue { get; set; }

            public string MemberName { get; set; }
        }

        private static class PredicateInfo
        {
            public static PredicateInfo<T> From<T>(Expression<Func<T, string>> predicate)
            {
                return new PredicateInfo<T>
                {
                    MemberName = ValidatorUtil.GetMemberName(predicate),
                    GetValue = predicate.MakeSafeGet()
                };
            }
        }
    }
}
