﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;

namespace BrightSpark.Validator
{
    public static class EstIdCodeExt
    {
        /// <summary>
        /// Add validator which checks if model property value is valid estonian id code
        /// </summary>
        [ValidatorId("3DF6AF21-A451-46B0-B928-D99E0CD93043")]
        public static IValidator<T> IsValidEstonianIdentityCode<T>(this IValidator<T> validator, Expression<Func<T, string>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            var key = ValidatorUtil.GetMemberName(predicate);
            var get = predicate.MakeSafeGet();
            return validator.IsValidEstonianIdentityCode(key, get, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property value is valid estonian id code
        /// </summary>
        [ValidatorId("45FCD1C3-2A38-4186-B732-61A27434B496")]
        public static IValidator<T> IsValidEstonianIdentityCode<T>(this IValidator<T> validator, string key, Func<T, string> get, string errorMessage = "*", int? errorCode = null)
        {
            return validator.IsTrue(key, x =>
            {
                var code = get(x);

                if (string.IsNullOrEmpty(code))
                {
                    return true;
                }

                if (code.Length != 11)
                {
                    return false;
                }

                // Check pattern
                if (!System.Text.RegularExpressions.Regex.IsMatch(code, @"^[1-6]\d{10}"))
                {
                    return false;
                }
                
                // Check birth date
                int icType = int.Parse(code[0].ToString(CultureInfo.InvariantCulture));
                string dateStr = (icType == 1 || icType == 2
                    ? "18"
                    : (icType == 3 || icType == 4 ? "19" : "20"));

                dateStr += code.Substring(1, 6);
                DateTime dateVal;
                if (!DateTime.TryParseExact(dateStr, "yyyyMMdd", null, DateTimeStyles.None, out dateVal))
                {
                    return false;
                }

                return GetIdentityCodeCheckSum(code) == int.Parse(code[10].ToString(CultureInfo.InvariantCulture));

            }, errorMessage, errorCode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        private static int GetIdentityCodeCheckSum(string code)
        {
            //Convert code characters to int list
            List<int> ik = code.ToCharArray().Select(x => Int32.Parse(x.ToString(CultureInfo.InvariantCulture))).ToList();
            int total = 0;
            // Do first run.
            for (int i = 0, j = 1; i < 10; j = (j == 9 ? 1 : j + 1), i++) { total += ik[i] * j; }
            int mod = total % 11;
            if (mod != 10) { return mod; }
            // If modulus is ten we need second run.
            total = 0;
            for (int i = 0, j = 3; i < 10; j = (j == 9 ? 1 : j + 1), i++) { total += ik[i] * j; }
            mod = total % 11;
            // If modulus is still ten then return 0.
            return mod == 10 ? 0 : mod;
        }
    
    }
}
