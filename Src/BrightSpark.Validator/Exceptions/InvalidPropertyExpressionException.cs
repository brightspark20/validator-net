﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrightSpark.Validator.Exceptions
{
    public class InvalidPropertyExpressionException : Exception
    {
        public InvalidPropertyExpressionException(string message) : base(message)
        {
        }
    }
}
