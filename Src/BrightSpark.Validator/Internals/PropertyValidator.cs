﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BrightSpark.Validator
{
    public class PropertyValidator<T>
    {
        public Func<T, string, IEnumerable<ValidationResult>> Validator { get; set; }
    }

    public class AsyncPropertyValidator<T> : PropertyValidator<T>
    {
        public new Func<T, string, Task<IEnumerable<ValidationResult>>> Validator { get; set; }
    }
}
