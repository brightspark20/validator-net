﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrightSpark.Validator
{
	public class ConditionalValidator<T>
	{
		public Func<T, bool> Condition { get; set; }
		public IValidator<T> Validator { get; set; }
	}

    public class AsyncConditionalValidator<T> : ConditionalValidator<T>
    {
        public Func<T, Task<bool>> ConditionAsync { get; set; }
    }
}
