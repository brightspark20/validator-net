﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using BrightSpark.Validator.Exceptions;

namespace BrightSpark.Validator
{
    internal static class ValidatorUtil
    {
        private const string DELIMITER = ".";

        public static string GetMemberName(LambdaExpression predicate)
        {
            var ex = predicate.Body;

            var builder = new System.Text.StringBuilder();
            while (true)
            {
                if (ex is MemberExpression memberExpression)
                {
                    builder
                        .Insert(0, memberExpression.Member.Name)
                        .Insert(0, DELIMITER);
                    ex = memberExpression.Expression;
                }
                else if (ex is UnaryExpression && ex.NodeType == ExpressionType.Convert)
                {
                    break;
                }
                else
                {
                    if (!(ex is ParameterExpression))
                    {
                        throw new InvalidPropertyExpressionException($"Expression {ex.NodeType} not allowed");
                    }

                    break;
                }
            }

            if (builder.Length == 0)
            {
                return string.Empty;
            }

            return builder.Remove(0, 1).ToString();
        }
    }

    internal static class ExpressionExtensions
    {
        public static Func<T, TProp> MakeSafeGet<T, TProp>(this Expression<Func<T, TProp>> exp)
        {
            var safePropertyExpression = GetSafePropertyExpression(exp.Body);

            var lambda = Expression.Lambda<Func<T, TProp>>(safePropertyExpression, exp.Parameters);

            return lambda.Compile();
        }

        private static Expression GetSafePropertyExpression(Expression propExpression)
        {
            var stack = GetMemberExpressionStack(propExpression as MemberExpression);
            stack.Reverse();

            var target = Expression.Label(propExpression.Type);
            var defaultValue = Expression.Constant(GetDefaultTypeValue(propExpression.Type), propExpression.Type);
            LabelExpression returnLabel = Expression.Label(target, defaultValue);
            var ex = GetSafePropertyExpression(stack, target, defaultValue);
            return Expression.Block(ex, returnLabel);
        }

        private static Expression GetSafePropertyExpression(List<Expression> valStack, LabelTarget target, ConstantExpression defaultValue, int i = 0)
        {
            var memberExpression = valStack[i];

            if (i == valStack.Count - 1)
            {
                return Expression.Return(target, memberExpression);
            }

            var isNullExpression = Expression.Equal(Expression.Constant(null), memberExpression);

            return Expression.IfThenElse(
                isNullExpression,
                Expression.Return(target, defaultValue),
                GetSafePropertyExpression(valStack, target, defaultValue, i + 1)
            );
        }

        private static List<Expression> GetMemberExpressionStack(MemberExpression propertyExpression)
        {
            var stack = new List<Expression>();
            Expression last = propertyExpression;
            while (propertyExpression != null)
            {
                stack.Add(propertyExpression);
                last = propertyExpression.Expression;
                propertyExpression = last as MemberExpression;
            }

            stack.Add(last);

            return stack;
        }
        public static object GetDefaultTypeValue(Type type)
        {
            return type.IsValueType ? Activator.CreateInstance(type) : null;
        }
    }
}
