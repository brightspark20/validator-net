using System.Collections.Generic;

namespace BrightSpark.Validator
{
    public class ValidationResult
    {
		public static ValidationResult OK => new ValidationResult();

        private readonly List<ValidationError> _errors = new List<ValidationError>();

        public virtual string Key { get; private set; }
        public ValidationError[] Errors => _errors.ToArray();

        public ValidationResult()
        {
        }

        public ValidationResult(ValidationError error)
        {
            AddError(error);
        }

        public ValidationResult(string error, int? code = null, int? level = null)
        {
            AddError(error, code, level);
        }

        public ValidationResult AddError(string error, int? code = null, int? level = null)
        {
            var validationError = new ValidationError(error, code, level);
            AddError(validationError);

			return this;
        }

        public ValidationResult AddError(ValidationError error)
        {
            _errors.Add(error);

			return this;
        }

		public ValidationResult AddErrors(IEnumerable<ValidationError> errors)
        {
            _errors.AddRange(errors);

			return this;
        }

		public static ValidationResult Ok(string key)
		{
			return new ValidationResult() { Key = key };
		}

		public static T Ok<T>(string key) where T: ValidationResult, new ()
        {
			return new T() { Key = key };
		}
    }
}