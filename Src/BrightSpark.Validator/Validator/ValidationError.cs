namespace BrightSpark.Validator
{
    public class ValidationError
    {
        public int Level { get; }
        public string Error { get; }
        public int? Code { get; }
        public object[] Args { get; }

        public ValidationError() { }
        public ValidationError(string error, int? code = null, int? level = null, params object[] args)
        {
            Error = error;
            Level = level ?? 0;
            Code = code;
            Args = args;
        }

        public static implicit operator ValidationError(string error)
        {
            return new ValidationError(error);
        }
    }
}