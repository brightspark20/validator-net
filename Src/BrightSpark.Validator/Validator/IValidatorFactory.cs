﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrightSpark.Validator
{
    public interface IValidatorFactory
    {
        IValidator<T> Create<T>();
    }

    public class DefaultValidatorFactory : IValidatorFactory
    {
        public IValidator<T> Create<T>()
        {
            return new Validator<T>();
        }
    }
}
