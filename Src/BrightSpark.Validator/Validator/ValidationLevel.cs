namespace BrightSpark.Validator
{
    /// <summary>
    /// Default is 0 - which is Error. 
    /// Errors are from 0 to -99
    /// Warnings are from -100 to -199
    /// </summary>
    public struct ValidationLevel
    {
        public static ValidationLevel Error = new ValidationLevel(0);
        public static ValidationLevel Warning = new ValidationLevel(-100);

        public readonly int Level;

        public ValidationLevel(int level)
        {
            this.Level = level;
        }

        public static implicit operator ValidationLevel(int i)
        {
            return new ValidationLevel(i);
        }
    }
}