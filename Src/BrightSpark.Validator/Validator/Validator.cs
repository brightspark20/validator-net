﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using static System.String;

namespace BrightSpark.Validator
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Validator<T> : IValidator<T>
    {
        /// <summary>
        /// 
        /// </summary>
        private HashSet<string> _excludedFields = new HashSet<string>();

        /// <summary>
        /// Current error level.
        /// Default is 0 - which is Error. 
        /// Errors are from 0 to -99
        /// Warnings are from -100 to -199
        /// </summary>
        private ValidationLevel _currentErrorLevel = ValidationLevel.Error;

        private readonly List<ConditionalValidator<T>> _conditionalValidators = new List<ConditionalValidator<T>>();

        /// <summary>
        /// 
        /// </summary>
        private readonly IDictionary<string, IList<PropertyValidator<T>>> _propertyValidators = new Dictionary<string, IList<PropertyValidator<T>>>();

        /// <summary>
        /// If true, all validations are executed.
        /// If false, properties validations will be executed until one fails.
        /// </summary>
        public bool ContinueOnError { get; set; }

        /// <summary>
        /// From this point on all property validators will have given level. 
        /// Levels from 0 to -99 are considered as errors.
        /// -99 to -.. are considered as warnings.
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public IValidator<T> SetLevel(ValidationLevel level)
        {
            _currentErrorLevel = level;
            return this;
        }

        /// <summary>
        /// Add validator which checks if model property is set (i.e not null)
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode">Error code</param>
        /// <returns></returns>
        public IValidator<T> Required<TProp>(Expression<Func<T, TProp>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            return Required(memberName, predicate, errorMessage, errorCode);
        }

        /// <summary>
        /// Add validator which checks if model property is set (i.e not null)
        /// </summary>
        /// <param name="key"></param>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode">Error code</param>
        /// <returns></returns>
        public IValidator<T> Required<TProp>(string key, Expression<Func<T, TProp>> predicate, string errorMessage = "*", int? errorCode = null)
        {
            return IsTrue(key, x =>
            {
                var get = predicate.MakeSafeGet();
                var value = get(x);

                var enumValue = value as Enum;
                if (enumValue != null)
                {
                    return (int) (object) enumValue > 0;
                }

                var stringValue = value as string;
                if (stringValue != null && stringValue == Empty)
                {
                    return false;

                }

                return value != null;
            }, errorMessage, errorCode);
        }

        public IValidator<T> IsTrueByValue<TProp>(
            Expression<Func<T, TProp>> predicate,
            Func<TProp, bool> valueValidator, 
            string errorMessage = "*", 
            int? errorCode = null)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);

            return IsTrue(memberName, x =>
            {
                var get = predicate.MakeSafeGet();
                var val = get(x);
                return valueValidator(val);

            }, errorMessage, errorCode);
        }



        public IValidator<T> IsTrueByValue<TProp>(string memberName, Func<T, TProp> get, Func<TProp, bool> valueValidator, string errorMessage = "*", int? errorCode = null)
        {
            return IsTrue(memberName, x =>
            {
                var val = get(x);
                return valueValidator(val);

            }, errorMessage, errorCode);
        }

        /// <inheritdoc />
        public IValidator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, Func<T, bool> customValidator, string errorMessage = "*", int? errorCode = null)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            return IsTrue(memberName, customValidator, errorMessage, errorCode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="customValidator"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        public IValidator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, Func<T, string, bool> customValidator, string errorMessage = "*", int? errorCode = null)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            return IsTrue(memberName, customValidator, errorMessage, errorCode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="customValidator"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        public IValidator<T> IsTrue(string key, Func<T, bool> customValidator, string errorMessage = "*", int? errorCode = null)
        {
            AddPropertyValidator(key, (model, k) => customValidator(model)
                ? new[] { ValidationResult.Ok(k) }
                : new[] { ValidationResult.Ok(k).AddError(errorMessage, errorCode, _currentErrorLevel.Level) });
            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="customValidator"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        public IValidator<T> IsTrue(string key, Func<T, string, bool> customValidator, string errorMessage = "*", int? errorCode = null)
        {
            AddPropertyValidator(key, (model, k) => customValidator(model, k)
                ? new[] { ValidationResult.Ok(k) }
                : new[] { ValidationResult.Ok(k).AddError(errorMessage, errorCode, _currentErrorLevel.Level) });
            return this;
        }

        public IValidator<T> IsTrue(string key, Func<T, string, IEnumerable<ValidationResult>> validator)
        {
            AddPropertyValidator(key, validator);
            return this;
        }

        [Obsolete("Use ValidateObject instead")]
        public IValidator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, IValidator<T1> validator)
        {
            return IsTrue(predicate, (x, key) =>
            {
                var val = x == null 
                    ? default(T1) 
                    : predicate.Compile().Invoke(x);

                var barSummary = validator.Validate(val);
                return barSummary.Results.Select(res =>
                    ValidationResult.Ok($"{key}.{res.Key}").AddErrors(res.Value.Errors)
                );
            });
        }

        public IValidator<T> IsTrueAsync<T1>(Expression<Func<T, T1>> predicate, IValidator<T1> validator)
        {
            return IsTrueAsync(predicate, async (x, key) =>
            {
                var val = x == null 
                    ? default(T1) 
                    : predicate.Compile().Invoke(x);

                var barSummary = await validator.ValidateAsync(val);
                return barSummary.Results.Select(res =>
                    ValidationResult.Ok($"{key}.{res.Key}").AddErrors(res.Value.Errors)
                );
            });
        }

        public IValidator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, Func<T, ValidationResult> validator)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            return IsTrue(memberName, validator);
        }

        public IValidator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, Func<T, IEnumerable<ValidationResult>> validator)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            return IsTrue(memberName, validator);
        }

        public IValidator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, Func<T, string, ValidationResult> validator)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            return IsTrue(memberName, validator);
        }

        public IValidator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, Func<T, string, IEnumerable<ValidationResult>> validator)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            return IsTrue(memberName, validator);
        }

        public IValidator<T> IsTrueAsync<T1>(Expression<Func<T, T1>> predicate, Func<T, Task<ValidationResult>> validator)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            return IsTrueAsync(memberName, validator);
        }

        public IValidator<T> IsTrueAsync<T1>(Expression<Func<T, T1>> predicate, Func<T, string, Task<ValidationResult>> validator)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            return IsTrueAsync(memberName, validator);
        }

        public IValidator<T> IsTrueAsync<T1>(Expression<Func<T, T1>> predicate, Func<T, Task<IEnumerable<ValidationResult>>> validator)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            return IsTrueAsync(memberName, validator);
        }

        public IValidator<T> IsTrueAsync<T1>(Expression<Func<T, T1>> predicate, Func<T, string, Task<IEnumerable<ValidationResult>>> validator)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            return IsTrueAsync(memberName, validator);
        }

        public IValidator<T> IsTrueAsync(string key, Func<T, Task<IEnumerable<ValidationResult>>> validator)
        {
            AddAsyncPropertyValidator(key, (x, k) => validator(x));
            return this;
        }

        public IValidator<T> IsTrueAsync(string key, Func<T, string, Task<IEnumerable<ValidationResult>>> validator)
        {
            AddAsyncPropertyValidator(key, validator);
            return this;
        }

        public IValidator<T> IsTrue(string key, Func<T, ValidationResult> validator)
        {
            return IsTrue(key, (x, k) => validator(x));
        }

        public IValidator<T> IsTrue(string key, Func<T, IEnumerable<ValidationResult>> validator)
        {
            AddPropertyValidator(key, (x, k) => validator(x));
            return this;
        }

        public IValidator<T> IsTrueAsync(string key, Func<T, Task<ValidationResult>> validator)
        {
            return IsTrueAsync(key, (x, k) => validator(x));
        }

        public IValidator<T> IsTrue(string key, Func<T, string, ValidationResult> validator)
        {
            AddPropertyValidator(key, (x, k) => {
                var result = validator(x, k);

                if (result.Key == null)
                {
                    result = ValidationResult
                        .Ok(k)
                        .AddErrors(result.Errors);
                }

                return new[] { result };
            });

            return this;
        }

        public IValidator<T> IsTrueAsync(string key, Func<T, string, Task<ValidationResult>> validator)
        {
            AddAsyncPropertyValidator(key, async (x, k) => {
                var result = await validator(x, k);

                if (result.Key == null)
                {
                    result = ValidationResult
                        .Ok(k)
                        .AddErrors(result.Errors);
                }

                return new[] { result };
            });

            return this;
        }

        public IValidator<T> IsTrueAsync<T1>(Expression<Func<T, T1>> predicate, Func<T, string, Task<bool>> customValidator, string errorMessage = "*", int? errorCode = null)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);
            return IsTrueAsync(memberName, customValidator, errorMessage, errorCode);
        }

        public IValidator<T> IsTrueAsync(string key, Func<T, string, Task<bool>> customValidator, string errorMessage = "*", int? errorCode = null)
        {
            AddAsyncPropertyValidator(key, async (model, k) =>
            {
                var res = await customValidator(model, k);
                return res
                    ? new[] { ValidationResult.Ok(k) }
                    : new[] { ValidationResult.Ok(k).AddError(errorMessage, errorCode, _currentErrorLevel.Level) };
            });
            return this;
        }

        public IValidator<T> WhenAsync(Func<T, Task<bool>> func, Func<IValidator<T>, IValidator<T>> validatorSetup)
        {
            var validator = validatorSetup(new Validator<T>());

            _conditionalValidators.Add(new AsyncConditionalValidator<T>
            {
                ConditionAsync = func,
                Validator = validator
            });

            return this;
        }

        public IValidator<T> WhenAsync(Func<T, bool> func, Func<IValidator<T>, IValidator<T>> validatorSetup)
        {
            var validator = validatorSetup(new Validator<T>());

            _conditionalValidators.Add(new AsyncConditionalValidator<T>
            {
                Condition = func,
                Validator = validator
            });

            return this;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="func"></param>
        /// <param name="validatorSetup"></param>
        /// <returns></returns>
        public IValidator<T> When(Func<T, bool> func, Func<IValidator<T>, IValidator<T>> validatorSetup)
        {
            var validator = validatorSetup(new Validator<T>());
            return When(func, validator);
        }

        public IValidator<T> When(Func<T, bool> func, IValidator<T> v)
        {
            _conditionalValidators.Add(new ConditionalValidator<T>
            {
                Condition = func,
                Validator = v
            });

            return this;
        }

        public IValidator<T> ValidateObject<T1>(Expression<Func<T, T1>> predicate, IValidator<T1> itemValidator, string errorMessage = null, int? errorCode = null)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);

            AddPropertyValidator(memberName, (model, rootKey) =>
            {
                var get = predicate.MakeSafeGet();
                var x = get(model);

                var itemValidationSummary = itemValidator.Validate(x);
                var res = new List<ValidationResult>();

                if (!itemValidationSummary.IsValid && !string.IsNullOrEmpty(errorMessage))
                {
                    res.Add(ValidationResult.Ok(rootKey).AddError(errorMessage, errorCode));
                }

                res.AddRange(itemValidationSummary.Results
                    .Select(validationResult =>
                        ValidationResult.Ok($"{rootKey}.{validationResult.Key}")
                            .AddErrors(validationResult.Value.Errors)
                    )
                );

                return res;
            });

            return this;
        }

        [Obsolete("Use ValidateCollection instead")]
        public IValidator<T> ValidCollection<T1>(Expression<Func<T, IEnumerable<T1>>> predicate, IValidator<T1> itemValidator, string errorMessage = "*", int? errorCode = null)
        {
            return ValidateCollection(predicate, itemValidator, errorMessage, errorCode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <param name="predicate"></param>
        /// <param name="itemValidator"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        public IValidator<T> ValidateCollection<T1>(Expression<Func<T, IEnumerable<T1>>> predicate, IValidator<T1> itemValidator, string errorMessage = "*", int? errorCode = null)
        {
            var memberName = ValidatorUtil.GetMemberName(predicate);

            AddPropertyValidator(memberName, (model, rootKey) =>
            {
                var get = predicate.MakeSafeGet();
                var collection = get(model) ?? new T1[0];

                //IEnumerable<T1> collection = model == null
                //    ? new T1[0]
                //    : predicate.Compile().Invoke(model);

                var itemsResultsSeed = collection
                    .Select((x, i) => new
                    {
                        Result = itemValidator.Validate(x),
                        Index = i
                    })
                    .ToArray();

                var res = new List<ValidationResult>();

                if (itemsResultsSeed.Any(x => !x.Result.IsValid))
                {
                    res.Add(ValidationResult.Ok(rootKey)
                        .AddError(errorMessage)
                    );
                }

                var itemsResults =  itemsResultsSeed
                    .SelectMany(x => x.Result.Results
                        .Select(y => ValidationResult
                            .Ok(IsNullOrEmpty(y.Key)
                                ? $"{rootKey}[{x.Index}]"
                                : $"{rootKey}[{x.Index}].{y.Key}"
                            )
                            .AddErrors(y.Value.Errors)));

                res.AddRange(itemsResults);
                return res;
            });

            return this;
        }

        ValidationSummary IValidator.Validate(object model)
        {
            return Validate((T)model);
        }

        Task<ValidationSummary> IValidator.ValidateAsync(object model)
        {
            return ValidateAsync((T)model);
        }

        public ValidationSummary Validate(T model)
        {
            var task = ValidateAsync(model);
            return task.Result;
        }

        public async Task<ValidationSummary> ValidateAsync(T model)
        {
            var res = new ValidationSummary(ValidatorDefaults.DefaultWarningLevel);

            var tasks = _conditionalValidators
                .Select(x => ValidateConditionalAsync(x, model));

            foreach (var task in tasks)
            {
                res.MergeWith(await task);
            }

            tasks = _propertyValidators
                .Where(x => !_excludedFields.Contains(x.Key))
                .Select(x => ValidatePropertyAsync(model, x.Key, x.Value));

            foreach (var task in tasks)
            {
                res.MergeWith(await task);
            }

            return res;
        }

        /// <summary>
        /// Register field validator
        /// </summary>
        private void AddPropertyValidator(string key, Func<T, string, IEnumerable<ValidationResult>> validator)
        {
            GetPropertyValidatorsList(key)
                .Add(new PropertyValidator<T> { Validator = validator });
        }

        private void AddAsyncPropertyValidator(string key, Func<T, string, Task<IEnumerable<ValidationResult>>> validator)
        {
            GetPropertyValidatorsList(key)
                .Add(new AsyncPropertyValidator<T> { Validator = validator });
        }

        private IList<PropertyValidator<T>> GetPropertyValidatorsList(string key)
        {
            if (!_propertyValidators.ContainsKey(key))
            {
                _propertyValidators[key] = new List<PropertyValidator<T>>();
            }

            return _propertyValidators[key];
        }

        //protected ValidationSummary ValidateConditional(ConditionalValidator<T> conditionalValidator, T model)
        //{
        //    return ValidateConditionalAsync(conditionalValidator, model).Result;

        //    //if (conditionalValidator is AsyncConditionalValidator<T> asyncConditionalValidator)
        //    //{
        //    //    var pass = asyncConditionalValidator.Condition != null
        //    //        ? asyncConditionalValidator.Condition(model)
        //    //        : asyncConditionalValidator.ConditionAsync(model).Result;

        //    //    if (pass)
        //    //    {
        //    //        if (asyncConditionalValidator.Validator != null)
        //    //        {
        //    //            return asyncConditionalValidator.Validator.Validate(model);
        //    //        }

        //    //        return asyncConditionalValidator.ValidatorAsync.ValidateAsync(model).Result;
        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    var pass = conditionalValidator.Condition(model);
        //    //    if (pass)
        //    //    {
        //    //        return conditionalValidator.Validator.Validate(model);
        //    //    }
        //    //}

        //    //return new ValidationSummary();
        //}

        protected async Task<ValidationSummary> ValidateConditionalAsync(ConditionalValidator<T> conditionalValidator, T model)
        {
            if (conditionalValidator is AsyncConditionalValidator<T> asyncConditionalValidator)
            {
                var pass = asyncConditionalValidator.Condition != null
                    ? asyncConditionalValidator.Condition(model)
                    : await asyncConditionalValidator.ConditionAsync(model);

                if (pass)
                {
                    return await asyncConditionalValidator.Validator.ValidateAsync(model);
                }
            }
            else
            {
                var pass = conditionalValidator.Condition(model);
                if (pass)
                {
                    return conditionalValidator.Validator.Validate(model);
                }
            }

            return new ValidationSummary();
        }

        //protected ValidationSummary ValidateProperty(T model, string key, IList<PropertyValidator<T>> propertyValidators)
        //{
        //    return ValidatePropertyAsync(model, key, propertyValidators).Result;

        //    //var summary = new ValidationSummary(WarningLevel);

        //    //foreach (var propertyValidator in propertyValidators)
        //    //{
        //    //    if (!ContinueOnError && summary.HasError(key))
        //    //    {
        //    //        continue;
        //    //    }

        //    //    var results = propertyValidator is AsyncPropertyValidator<T> asyncPropertyValidator
        //    //        ? asyncPropertyValidator.Validator(model, key).Result
        //    //        : propertyValidator.Validator(model, key);

        //    //    foreach (var result in results)
        //    //    {
        //    //        summary = summary.AddErrors(result.Key, result.Errors);
        //    //    }
        //    //}

        //    //return summary;
        //}

        protected async Task<ValidationSummary> ValidatePropertyAsync(T model, string key, IList<PropertyValidator<T>> propertyValidators)
        {
            var summary = new ValidationSummary(ValidatorDefaults.DefaultWarningLevel);

            foreach (var propertyValidator in propertyValidators)
            {
                if (!ContinueOnError && summary.HasError(key))
                {
                    continue;
                }

                var results = propertyValidator is AsyncPropertyValidator<T> asyncPropertyValidator
                    ? await asyncPropertyValidator.Validator(model, key)
                    : propertyValidator.Validator(model, key);

                summary = results.Aggregate(summary, (current, result) => current.AddErrors(result.Key, result.Errors));
            }

            return summary;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="excludeFields"></param>
        public void SetExcludedFields(IEnumerable<string> excludeFields)
        {
            _excludedFields = new HashSet<string>(excludeFields ?? Enumerable.Empty<string>());
        }

        public bool IsValid(T obj, out ValidationSummary summary)
        {
            summary = Validate(obj);
            return summary.IsValid;
        }
    }
}