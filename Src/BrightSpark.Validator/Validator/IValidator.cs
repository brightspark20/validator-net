﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace BrightSpark.Validator
{
    public interface IValidator
    {
        ValidationSummary Validate(object model);
        Task<ValidationSummary> ValidateAsync(object model);
    }

    public interface IValidator<T> : IValidator
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        ValidationSummary Validate(T model);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<ValidationSummary> ValidateAsync(T model);

        //TRes Required<TProp>(Expression<Func<T, TProp>> predicate, string errorMessage = "*", int? errorCode = null);
        /// <summary>
        /// Add validator which checks if model property is set (i.e not null)
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode">Error code</param>
        /// <returns></returns>
        IValidator<T> Required<TProp>(Expression<Func<T, TProp>> predicate, string errorMessage = "*", int? errorCode = null);

        /// <summary>
        /// Add validator which checks if model property is set (i.e not null)
        /// </summary>
        /// <param name="key"></param>
        /// <param name="predicate"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode">Error code</param>
        /// <returns></returns>
        IValidator<T> Required<TProp>(string key, Expression<Func<T, TProp>> predicate, string errorMessage = "*", int? errorCode = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="customValidator"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        IValidator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, Func<T, bool> customValidator, string errorMessage = "*", int? errorCode = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="customValidator"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        IValidator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, Func<T, string, bool> customValidator, string errorMessage = "*", int? errorCode = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="customValidator"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        IValidator<T> IsTrue(string key, Func<T, bool> customValidator, string errorMessage = "*", int? errorCode = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="customValidator"></param>
        /// <param name="errorMessage"></param>
        /// <param name="errorCode"></param>
        /// <returns></returns>
        IValidator<T> IsTrue(string key, Func<T, string, bool> customValidator, string errorMessage = "*", int? errorCode = null);

        IValidator<T> IsTrue(string key, Func<T, ValidationResult> validator);
        IValidator<T> IsTrue(string key, Func<T, IEnumerable<ValidationResult>> validator);
        IValidator<T> IsTrue(string key, Func<T, string, ValidationResult> validator);
        IValidator<T> IsTrue(string key, Func<T, string, IEnumerable<ValidationResult>> validator);

        [Obsolete("Use ValidateObject instead")]
        IValidator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, IValidator<T1> validator);

        IValidator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, Func<T, ValidationResult> validator);
        IValidator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, Func<T, IEnumerable<ValidationResult>> validator);
        IValidator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, Func<T, string, ValidationResult> validator);
        IValidator<T> IsTrue<T1>(Expression<Func<T, T1>> predicate, Func<T, string, IEnumerable<ValidationResult>> validator);

        IValidator<T> IsTrueAsync(string key, Func<T, Task<ValidationResult>> validator);
        IValidator<T> IsTrueAsync(string key, Func<T, Task<IEnumerable<ValidationResult>>> validator);
        IValidator<T> IsTrueAsync(string key, Func<T, string, Task<ValidationResult>> validator);
        IValidator<T> IsTrueAsync(string key, Func<T, string, Task<IEnumerable<ValidationResult>>> validator);

        IValidator<T> IsTrueAsync<T1>(Expression<Func<T, T1>> predicate, IValidator<T1> validator);
        IValidator<T> IsTrueAsync<T1>(Expression<Func<T, T1>> predicate, Func<T, Task<ValidationResult>> validator);
        IValidator<T> IsTrueAsync<T1>(Expression<Func<T, T1>> predicate, Func<T, Task<IEnumerable<ValidationResult>>> validator);
        IValidator<T> IsTrueAsync<T1>(Expression<Func<T, T1>> predicate, Func<T, string, Task<ValidationResult>> validator);
        IValidator<T> IsTrueAsync<T1>(Expression<Func<T, T1>> predicate, Func<T, string, Task<IEnumerable<ValidationResult>>> validator);

        IValidator<T> IsTrueAsync(string key, Func<T, string, Task<bool>> customValidator, string errorMessage = "*", int? errorCode = null);
        IValidator<T> IsTrueAsync<T1>(Expression<Func<T, T1>> predicate, Func<T, string, Task<bool>> customValidator, string errorMessage = "*", int? errorCode = null);

        IValidator<T> IsTrueByValue<TProp>(Expression<Func<T, TProp>> predicate, Func<TProp, bool> valueValidator, string errorMessage = "*", int? errorCode = null);
        IValidator<T> IsTrueByValue<TProp>(string key, Func<T, TProp> get, Func<TProp, bool> valueValidator, string errorMessage = "*", int? errorCode = null);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="func"></param>
        /// <param name="validatorSetup"></param>
        /// <returns></returns>
        IValidator<T> When(Func<T, bool> func, Func<IValidator<T>, IValidator<T>> validatorSetup);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="func"></param>
        /// <param name="validatorSetup"></param>
        /// <returns></returns>
        IValidator<T> WhenAsync(Func<T, Task<bool>> func, Func<IValidator<T>, IValidator<T>> validatorSetup);

        IValidator<T> WhenAsync(Func<T, bool> func, Func<IValidator<T>, IValidator<T>> validatorSetup);

        /// <summary>
        /// From this point on all property validators will have given level. 
        /// Levels from 0 to -99 are considered as errors.
        /// -99 to -.. are considered as warnings.
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        IValidator<T> SetLevel(ValidationLevel level);

        /// <summary>
        /// Set sub-validator for property
        /// </summary>
        IValidator<T> ValidateObject<T1>(Expression<Func<T, T1>> predicate, IValidator<T1> itemValidator, string errorMessage = null, int? errorCode = null);

        /// <summary>
        /// Set sub-validator for collection
        /// </summary>
        IValidator<T> ValidateCollection<T1>(Expression<Func<T, IEnumerable<T1>>> predicate, IValidator<T1> itemValidator, string errorMessage = "*", int? errorCode = null);

        [Obsolete("Use ValidateCollection instead")]
        IValidator<T> ValidCollection<T1>(Expression<Func<T, IEnumerable<T1>>> predicate, IValidator<T1> itemValidator, string errorMessage = "*", int? errorCode = null);

        IValidator<T> When(Func<T, bool> func, IValidator<T> v);
    }
}
