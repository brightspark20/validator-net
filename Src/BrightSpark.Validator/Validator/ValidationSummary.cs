using System.Collections.Generic;
using System.Linq;

namespace BrightSpark.Validator
{
    public class ValidationSummary
    {
        private readonly int _warningLevel;

        private readonly IDictionary<string, ValidationResult> _results = new Dictionary<string, ValidationResult>();

        public IDictionary<string, ValidationResult> Results => _results.ToDictionary(x => x.Key, x => x.Value);

        public bool IsValid => ErrorMessages.Count == 0;

        public IDictionary<string, string> ErrorMessages => _results
            .Select(x => new {
                x.Key,
                Error = x.Value.Errors
                    .FirstOrDefault(err => err.Level > _warningLevel && err.Level <= 0)?.Error
            })
            .Where(x => x.Error != null)
            .ToDictionary(x => x.Key, x => x.Error);

        public IDictionary<string, string> WarningMessages => _results
            .Select(x => new {
                x.Key,
                Warning = x.Value.Errors
                    .FirstOrDefault(err => err.Level <= _warningLevel)?.Error
            })
            .Where(x => x.Warning != null)
            .ToDictionary(x => x.Key, x => x.Warning);
        
        public ValidationSummary() : this(ValidatorDefaults.DefaultWarningLevel)
        {
        }

        public ValidationSummary(int warningLevel)
        {
            _warningLevel = warningLevel;
        }

        public ValidationSummary AddResult(string key, ValidationResult result)
        {
            _results[key] = result;

            return this;
        }

        public ValidationSummary MergeWith(ValidationSummary summary)
        {
            foreach (var kvp in summary._results)
            {
                if (!_results.ContainsKey(kvp.Key))
                {
                    AddResult(kvp.Key, kvp.Value);
                }
                else
                {
                    foreach (var validationError in kvp.Value.Errors)
                    {
                        AddError(kvp.Key, validationError);
                    }
                }
            }

            return this;
        }

        public ValidationSummary AddError(string key, string errorMessage, int level = 0, int? errorCode = null)
        {
            return AddError(key, new ValidationError(errorMessage, errorCode, level));
        }

        public ValidationSummary AddError(string key, ValidationError error)
        {
            return AddErrors(key, new[] { error });
        }

        public ValidationSummary AddErrors(string key, IEnumerable<ValidationError> errors)
        {
            foreach (var error in errors)
            {
                if (!_results.ContainsKey(key))
                {
                    _results[key] = ValidationResult.Ok(key);
                }

                _results[key].AddError(error);
            }

            return this;
        }

        public bool HasError(string key)
        {
            return ErrorMessages.ContainsKey(key);
        }
    }
}