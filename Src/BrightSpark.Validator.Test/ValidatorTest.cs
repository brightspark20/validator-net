﻿using System;
using System.Globalization;
using System.Linq;
using System.Threading;
using NUnit.Framework;
using System.Threading.Tasks;

namespace BrightSpark.Validator.Test
{
    [TestFixture]
    public class ValidatorTest
    {
        public enum Status
        {
            Unknown = 0,
            Active = 1,
            Disabled = 2
        }

        public class Foo
        {
            public string Name { get; set; }

            public bool HasStuff { get; set; }
            public string Stuff { get; set; }

            public bool HasOtherStuff { get; set; }
            public string OtherStuff { get; set; }

            public Status Status { get; set; }
            public string NumericString { get; set; }
        }

        [Test]
        public void ErrorCodes()
        {
            var validtor = new Validator<Foo>()
                .Required(x => x.Name, "Name is required", -1);


            var model = new Foo();
			var summary = validtor.Validate(model);
            if (!summary.IsValid)
            {
                foreach (var kvp in summary.Results)
                {
                    Console.WriteLine("{0}\t{1}\t{2}", kvp.Key, kvp.Value.Errors[0].Error, kvp.Value.Errors[0].Code);
                }

                Assert.AreEqual(1, summary.Results.Count);
                Assert.AreEqual(-1, summary.Results["Name"].Errors[0].Code);

                return;
            }
            Assert.Fail();
        }

        [Test]
        public void ValidationExceptionTest()
        {
            var validator = new Validator<Foo>()
                .IsTrue(x => x.Name, x =>
                {
                    return new ValidationResult("O'oh, wrong name!", 123, 0);
                });

            var model = new Foo();
            
            var summary = validator.Validate(model);

            Assert.AreEqual(1, summary.Results.Count);
            Assert.AreEqual(1, summary.Results["Name"].Errors.Length);
            Assert.AreEqual("O'oh, wrong name!", summary.Results["Name"].Errors[0].Error);
            Assert.AreEqual(123, summary.Results["Name"].Errors[0].Code);
            Assert.AreEqual(0, summary.Results["Name"].Errors[0].Level);

        }

        [Test]
        public void ConditionalValidationTest()
        {
            var validator = new Validator<Foo>()
                .When(x => x.HasStuff, v => v
                    .Required(x => x.Stuff, "Stuff required", 1000)
                ); 

            var foo = new Foo { HasStuff = true };
            var summary = validator.Validate(foo);

            Assert.IsFalse(summary.IsValid);

            var res = summary.Results;

            Assert.AreEqual(1, res["Stuff"].Errors.Length);
            Assert.AreEqual(1000, res["Stuff"].Errors[0].Code);

        }

        [Test]
        public void NestedConditionalValidationTest()
        {
            var validator = new Validator<Foo>()
                .When(x => x.HasStuff, v => v
                    .Required(x => x.Stuff, "Stuff required", 1000)
                    .When(x => x.HasOtherStuff, v2 => v2
                        .Required(x => x.OtherStuff)
                    )
                );

            var foo = new Foo { HasStuff = false };
            var summary = validator.Validate(foo);

            Assert.IsTrue(summary.IsValid);

            foo.HasStuff = true;
            foo.HasOtherStuff = true;

            summary = validator.Validate(foo);
            Assert.IsFalse(summary.IsValid);

            var res = summary.Results;
            Assert.AreEqual(2, res.Keys.Count);
        }

        [Test]
        public void IsTrueWithValidationResult()
        {
            var validator = new Validator<Foo>()
                .IsTrue(x => x.Name, x =>
                {
                    if (x.Name == "x")
                    {
                        return ValidationResult.OK;
                    }

                    return new ValidationResult("shiiiit");
                });


            var summary  = validator.Validate(new Foo {Name = "y"});
            Assert.IsFalse(summary.IsValid);

            var res = summary.Results;
            foreach (var validationResult in res)
            {
                Console.WriteLine($"validationResult.Key : {string.Join("; ", validationResult.Value.Errors.Select(x => x.Error))}");
            }
        }

		[Test]
        public void IsThreadSafe()
        {
            var validator = new Validator<Foo>()
                .When(x => x.HasStuff, v => v
                    .Required(x => x.Stuff, "Stuff required", 1000)
                    .When(x => x.HasOtherStuff, v2 => v2
                        .Required(x => x.OtherStuff)
                    )
                );

			var tasks = Enumerable
				.Range(0, 100)
				.Select(x => Task.Run(() => {
					var foo = new Foo { HasStuff = false };
					var summary = validator.Validate(foo);

					Assert.IsTrue(summary.IsValid);

					foo.HasStuff = true;
					foo.HasOtherStuff = true;

					summary = validator.Validate(foo);
					Assert.IsFalse(summary.IsValid);

					var res = summary.Results;
					Assert.AreEqual(2, res.Keys.Count);
				}))
				.ToArray();

			Task.WaitAll(tasks);
        }

        [Test]
        public void WarningsTest()
        {
            var validator = new Validator<Foo>()
                .SetLevel(ValidationLevel.Warning)
                .When(x => x.HasStuff, v => v
                    .SetLevel(ValidationLevel.Warning)
                    .Required(x => x.Stuff, "Stuff required")
                ); 

            var foo = new Foo { HasStuff = true };
            var summary = validator.Validate(foo);

            Assert.IsTrue(summary.IsValid);

            Assert.IsTrue(!summary.ErrorMessages.ContainsKey("Stuff"));
            Assert.IsTrue(summary.WarningMessages.ContainsKey("Stuff"));

        }

        [Test]
        public void Enum_IsRequired_Should_ReturnError_When_EnumValueIsZero()
        {
            var validator = new Validator<Foo>()
                .Required(x => x.Status);

            var model = new Foo();

            var sum = validator.Validate(model);

            Assert.IsFalse(sum.IsValid);
        }

        [Test]
        public void Enum_IsRequired_Should_BeValid_When_EnumValueIsNotZero()
        {
            var validator = new Validator<Foo>()
                .Required(x => x.Status);

            var model = new Foo
            {
                Status = Status.Active
            };

            var sum = validator.Validate(model);

            Assert.IsTrue(sum.IsValid);
        }


        [Test]
        public void IsNumeric_Should_AcceptBothDecimalSeparators()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("et");

            var validator = new Validator<Foo>()
                .IsNumeric(x => x.NumericString);

            var model1 = new Foo{ NumericString = "1,1" };
            var model2 = new Foo{ NumericString = "1.1" };

            Assert.IsTrue(validator.Validate(model1).IsValid);
            Assert.IsTrue(validator.Validate(model2).IsValid);
        }

        [Test]
        public void CustomValidationError_Should_RemainCustomAfterValidation()
        {
            var validator = new Validator<Foo>()
                .IsTrue(x => x.NumericString, (x, k) => {
                    return ValidationResult.Ok(k).AddError(new CustomError("test"));
                });

            var res = validator.Validate(new Foo());
            Assert.IsTrue(res.Results["NumericString"].Errors[0] is CustomError);
        }

        public class CustomError : ValidationError
        {
            public CustomError()
            {
            }

            public CustomError(string error, int? code = null, int? level = null) : base(error, code, level)
            {
            }
        }
    }
}