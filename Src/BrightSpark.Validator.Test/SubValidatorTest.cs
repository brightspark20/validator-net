﻿using System;
using System.Linq;
using NUnit.Framework;

namespace BrightSpark.Validator.Test
{
    [TestFixture]
    public class SubValidatorTest
    {
        public class Foo
        {
            public string Id { get; set; }
            public Bar Bar { get; set; }
        }

        public class Bar
        {
            public string Name { get; set; }
            public string Value { get; set; }
        }

        [Test]
        public void Validator_Should_AddPropertyNameAsErrorPrefix_When_ValidatingPropertyWithSubValidator()
        {
            var barValidator = new Validator<Bar>()
                .Required(x => x.Name)
                .Required(x => x.Value);

            var fooValidator = new Validator<Foo>()
                .Required(x => x.Id)
                .ValidateObject(x => x.Bar, barValidator);

            var summary = fooValidator.Validate(new Foo());

            foreach (var validationResult in summary.Results)
            {
                Console.WriteLine($"{validationResult.Key}: \n{string.Join("\n", validationResult.Value.Errors.Select(x => x.Error))}\n");
            }

            Assert.IsFalse(summary.IsValid);
            Assert.AreEqual(3, summary.ErrorMessages.Count);
            Assert.IsTrue(summary.ErrorMessages.ContainsKey("Id"));
            Assert.IsTrue(summary.ErrorMessages.ContainsKey("Bar.Name"));
            Assert.IsTrue(summary.ErrorMessages.ContainsKey("Bar.Value"));
        }

    }
}