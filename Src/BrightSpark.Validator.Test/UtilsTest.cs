﻿using System;
using System.Linq.Expressions;
using BrightSpark.Validator.Exceptions;
using NUnit.Framework;

namespace BrightSpark.Validator.Test
{
    [TestFixture]
    public class UtilsTest
    {
        public class Foo
        {
            public FooBar FooBar { get; set; }
        }

        public class FooBar
        {
            public Bar Bar { get; set; }
        }

        public class Bar
        {
            public int Id { get; set; }

            public string StringValue { get; set; }
        }

        private string GetMemberName<TProp>(Expression<Func<Foo, TProp>> ex)
        {
            return ValidatorUtil.GetMemberName(ex);
        }

        [Test]
        public void SafeGetExpressionExtension_Should_CreateFuncThatReturnsDefaultValue_If_ReturnTypeIsValueType()
        {
            Expression<Func<Foo, int>> exp = x => x.FooBar.Bar.Id;

            var safeGet = exp.MakeSafeGet();

            var res = safeGet(new Foo());

            Assert.AreEqual(0, res);
        }

        [Test]
        public void SafeGetExpressionExtension_Should_CreateFuncThatReturnsNull_If_ReturnTypeIsNotValueType()
        {
            Expression<Func<Foo, string>> exp = x => x.FooBar.Bar.StringValue;

            var safeGet = exp.MakeSafeGet();

            var res = safeGet(null);

            Assert.AreEqual(null, res);
        }

        [Test]
        public void GetMemberName_Should_ReturnNestedPropertiesSeparatedWithDot()
        {
            var memberName = GetMemberName(x => x.FooBar);
            Console.WriteLine(memberName);
            Assert.AreEqual("FooBar", memberName);

            memberName = GetMemberName(x => x.FooBar.Bar);
            Console.WriteLine(memberName);
            Assert.AreEqual("FooBar.Bar", memberName);

            memberName = GetMemberName(x => x.FooBar.Bar.Id);
            Console.WriteLine(memberName);
            Assert.AreEqual("FooBar.Bar.Id", memberName);
        }

        [Test]
        public void GetMemberName_Should_ThrowException_When_NotPropertyExpressionIsProvided()
        {
            try
            {
                GetMemberName(x => x.FooBar.ToString());
                Assert.Fail();
            }
            catch (Exception e)
            {
                Console.Write(e);
                Assert.IsTrue(e is InvalidPropertyExpressionException);
            }
        }
    }
}
