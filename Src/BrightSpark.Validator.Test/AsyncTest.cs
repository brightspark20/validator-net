﻿using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace BrightSpark.Validator.Test
{
    public class AsyncTest
    {
        public class Foo
        {
            public string Name { get; set; }
        }

        [Test]
        public async Task IsTrueCanHaveAsyncCheck()
        {
            var validator = new Validator<Foo>()
                .IsTrueAsync(x => x.Name, (x, s) =>
                {
                    return Task.Run(() =>
                    {
                        Thread.Sleep(1000);
                        return true;
                    });
                });

            var res = await validator.ValidateAsync(new Foo());
            Assert.IsTrue(res.IsValid);
        }

        [Test]
        public void AsyncValidationsCanBeCheckedSynchronously()
        {
            var validator = new Validator<Foo>()
                .IsTrueAsync(x => x.Name, (x, s) =>
                {
                    return Task.Run(() =>
                    {
                        Thread.Sleep(1000);
                        return true;
                    });
                });

            var res = validator.Validate(new Foo());
            Assert.IsTrue(res.IsValid);
        }
    }
}