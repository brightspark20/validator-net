﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using NUnit.Framework;

namespace BrightSpark.Validator.Test
{
    [TestFixture]
    public class SerializeTest
    {
        public class Foo
        {
            public string PropA { get; set; }
            public string PropB { get; set; }
        }

        [Test]
        public void IsValidationResultSerializable()
        {

            var validator = new Validator<Foo> {ContinueOnError = true};
            validator
                .Required(x => x.PropA, "Field is required", -101)
                .IsNumeric(x => x.PropA, "Field must be numeric", -102)
                .Required(x => x.PropB, "Field is also required", -201);

            var res = validator.Validate(new Foo());

            var json = JsonConvert.SerializeObject(res, Formatting.Indented);
            Console.Write(json);

        }
    }
}
