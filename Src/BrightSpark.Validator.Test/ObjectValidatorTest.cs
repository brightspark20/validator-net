﻿using System;
using System.Linq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace BrightSpark.Validator.Test
{
    [TestFixture]
    public class ObjectValidatorTest
    {
        public class Foo
        {
            public string Name { get; set; }
        }

        public class Bar
        {
            public string Id { get; set; }
            public string Name { get; set; }

            public Foo Foo { get; set; }
            public Bar ChildBar { get; set; }
        }


        public class Bar2
        {
            public Bar TheBar { get; set; }
        }

        [Test]
        public void ObjectValidatorShouldValidate()
        {
            var fooValidator = new Validator<Foo>()
                .Required(x => x.Name);

            var barValidator = new Validator<Bar>()
                .Property(x => x.ChildBar.Foo).Is(p => p
                    .ValidObject(fooValidator, "foo was not valid!")
                );

            var res = barValidator.Validate(new Bar());

            Assert.False(res.IsValid);

            Console.Write(JsonConvert.SerializeObject(res));
        }

        [Test]
        public void ValidateProperty()
        {
            var barValidator = new Validator<Bar>()
                .Required(x => x.Id, "Id is required")
                .IsNumeric(x => x.Id, "Id must be numeric")
                .Required(x => x.Name, "Name is required");

            var validator = new Validator<Bar2>()
                .ValidateObject(x => x.TheBar, barValidator);


            var model = new Bar2
            {
                TheBar = new Bar { Id = "2a" }
            };

            var summary = validator.Validate(model);
            var res = summary.Results.Values.ToArray();

            Assert.IsFalse(summary.IsValid);

            var expectedKeys = new[]
            {
                "TheBar.Id",
                "TheBar.Name",
            };

            var i = 0;
            foreach (var val in res)
            {
                foreach (var err in val.Errors)
                {
                    Console.WriteLine("{0}:\t{1}", val.Key, err.Error);
                    Assert.AreEqual(expectedKeys[i++], val.Key);
                }
            }

            Assert.AreEqual(2, res.Length);
        }
    }
}