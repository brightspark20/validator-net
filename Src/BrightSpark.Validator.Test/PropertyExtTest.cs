using System;
using Newtonsoft.Json;
using NUnit.Framework;

namespace BrightSpark.Validator.Test
{
    [TestFixture]
    public class PropertyExtTest
    {
        public class Foo
        {
            public string StringValue { get; set; }
            public int IntValue { get; set; }
            public int IntValue2 { get; set; }

        }

        [Test]
        public void X()
        {
            var validator = new Validator<Foo>()

                .Property(x => x.StringValue).Is(p => p
                    .NotNullOrEmpty("Not set")
                    //.StartsWith("X", "Should start with '{0}'")
                    .TrueByValue(x => x.StartsWith("X"), "Should start with 'X'")
                )
                .Property(x => x.IntValue).Is(p => p
                    .GreaterThan(1, "Should be greater than {1}")
                    .GreaterThanOrEqualTo(x => x.IntValue2, "Should be greater thant IntValue2({1})")
                )
                .Property(x => x.IntValue2).Is(p => p
                    .LessThan(x => x.IntValue, "Should be less than {1}")
                    .LessThan(1, "Should be less than {1}")
                );

            var res = validator.Validate(new Foo
            {
                IntValue2 = 1
            });
            Console.WriteLine(JsonConvert.SerializeObject(res, Formatting.Indented));
        }
    }
}