﻿using System;
using System.Linq;
using Newtonsoft.Json;
using NUnit.Framework;

namespace BrightSpark.Validator.Test
{
    [TestFixture]
    public class CollectionValidatorTest
    {
        public class Foo
        {
            public Bar[] Bars { get; set; }
            public string Name { get; set; }
            public Foo ChildFoo { get; set; }
        }

        public class Bar
        {
            public string Id { get; set; }
            public string Name { get; set; }
        }

        public class Foo2
        {
            public Bar2[] Bar2s { get; set; }
        }

        public class Bar2
        {
            public Bar TheBar { get; set; }
            public Bar[] Bars { get; set; }
        }

        public class ObjectWithSimpleCollection
        {
            public string[] Names { get; set; }
        }


        [Test]
        public void ObjectValidatorShouldValidate()
        {
            var itemValidator = new Validator<Bar>()
                .Required(x => x.Id, "id required")
                .Required(x => x.Name, "name required");

            var fooValidator = new Validator<Foo>()
                //.ValidateCollection(x => x.Bars, itemValidator, "bars collection vas invalid")
                .Collection(x => x.ChildFoo.Bars).Is(p => p
                    .Required("bars collection not set")
                    .ValidCollection(itemValidator, "bars collection was invalid")
                );

            var res = fooValidator.Validate(new Foo
            {
                Bars = new [] { new Bar() }
            });

            Assert.False(res.IsValid);

            Console.Write(JsonConvert.SerializeObject(res));
        }

        [Test]
        public void CollectionValidation()
        {
            var barValidator = new Validator<Bar>()
                .Required(x => x.Id, "Id is required")
                .IsNumeric(x => x.Id, "Id must be numeric")
                .Required(x => x.Name, "Name is required");

            var validator = new Validator<Foo>()
                .ValidateCollection(x => x.Bars, barValidator, "Invalid collection");

            var model = new Foo
            {
                Bars = new[]
                {
                    new Bar {Id = "1", Name = "test"},
                    new Bar {Id = "asdf"},
                    new Bar(),
                }
            };

            var summary = validator.Validate(model);
            var res = summary.Results;
            Assert.IsFalse(summary.IsValid);


            foreach (var error in res)
            {
                foreach (var errorText in error.Value.Errors)
                {
                    Console.WriteLine("{0}:{1}", error.Key, errorText.Error);
                }
            }

            Assert.AreEqual(5, res.Count);
        }

        [Test]
        public void MultilevelCollectionValidation()
        {
            var barValidator = new Validator<Bar>()
                .Required(x => x.Id, "Id is required")
                .IsNumeric(x => x.Id, "Id must be numeric")
                .Required(x => x.Name, "Name is required");

            var bar2Validator = new Validator<Bar2>()
                .ValidateCollection(x => x.Bars, barValidator, "Bars collection is invalid");

            var validator = new Validator<Foo2>()
                .ValidateCollection(x => x.Bar2s, bar2Validator, "Bar2s collection is invalid");

            var model = new Foo2
            {
                Bar2s = new[]
                {
                    new Bar2
                    {
                        Bars = new[]
                        {
                            new Bar {Id = "2", Name = "test3"},
                            new Bar {Id = "asdf"},
                        }
                    },
                    new Bar2
                    {
                        Bars = new[]
                        {
                            new Bar {Id = "1", Name = "test"},
                            new Bar(),
                        }
                    },
                }
            };

            var summary = validator.Validate(model);
            var res = summary.Results.Values.ToArray();

            Assert.IsFalse(summary.IsValid);

            var expectedKeys = new[]
            {
                "Bar2s",
                "Bar2s[0].Bars",
                "Bar2s[0].Bars[1].Id",
                "Bar2s[0].Bars[1].Name",
                "Bar2s[1].Bars",
                "Bar2s[1].Bars[1].Id",
                "Bar2s[1].Bars[1].Name"
            };

            var i = 0;
            foreach (var val in res)
            {
                foreach (var err in val.Errors)
                {
                    Console.WriteLine("{0}:\t{1}", val.Key, err.Error);
                    Assert.AreEqual(expectedKeys[i++], val.Key);
                }
            }

            Assert.AreEqual(7, res.Length);
        }

        [Test]
        public void SimpleCollectionValidation()
        {
            var nameValidator = new Validator<string>()
                .Required(x => x, "Name required");

            var validator = new Validator<ObjectWithSimpleCollection>()
                .ValidateCollection(x => x.Names, nameValidator, "Some of the names were invalid");

            var res = validator.Validate(new ObjectWithSimpleCollection
            {
                Names = new[] {""}
            });

            Assert.IsFalse(res.IsValid);

            var expectedKeys = new[]
            {
                "Names",
                "Names[0]"
            };

            var i = 0;
            foreach (var val in res.Results.Values)
            {
                foreach (var err in val.Errors)
                {
                    Console.WriteLine("{0}:\t{1}", val.Key, err.Error);
                    Assert.AreEqual(expectedKeys[i++], val.Key);
                }
            }
        }
    }
}