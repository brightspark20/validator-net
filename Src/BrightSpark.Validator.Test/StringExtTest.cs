using System;
using NUnit.Framework;

namespace BrightSpark.Validator.Test
{
    [TestFixture]
    public class StringExtTest
    {
        public class Foo
        {
            public string StringValue { get; set; }
            public Bar Bar { get; set; }
        }

        public class Bar
        {
            public string StringValue { get; set; }
        }

        [TestCase("info@some.domain", true)]
        [TestCase("INFO@some.domain", true)]
        [TestCase("Info@Some.Domain", true)]
        [TestCase("maxLego@gmail.COM", true)]
        [TestCase("maxlego+test@gmail.com", true)]
        [TestCase("maxlego+test@gmail", false)]
        [TestCase("@", false)]
        [TestCase("@com.com", false)]
        [TestCase("email", false)]
        public void Email_Should_Validate(string email, bool isValid)
        {
            var validator = new Validator<Foo>()
                .IsEmail(x => x.StringValue);

            var model = new Foo { StringValue = email };
            var summary = validator.Validate(model);

            Assert.AreEqual(isValid, summary.IsValid);
        }

        [Test]
        public void IsNumeric_Should_PassDecimal_When_UsingComma()
        {
            var validator = new Validator<Foo>()
                .IsNumeric(x => x.StringValue);

            var model = new Foo { StringValue = "1,4" };
            var summary = validator.Validate(model);

            Assert.IsTrue(summary.IsValid);
        }

        [Test]
        public void IsNumeric_Should_PassDecimal_When_UsingDot()
        {
            var validator = new Validator<Foo>()
                .IsNumeric(x => x.StringValue);

            var model = new Foo { StringValue = "1.4" };
            var summary = validator.Validate(model);

            Assert.IsTrue(summary.IsValid);
        }


        [Test]
        public void IsNotNullOrEmpty_Null()
        {
            var validator = new Validator<Foo>()
                .IsNotNullOrEmpty(x => x.StringValue);

            var model = new Foo();
            var summary = validator.Validate(model);

            Assert.IsFalse(summary.IsValid);
            var res = summary.Results;

            Console.WriteLine("Errors: {0}", res.Count);
        }

        [Test]
        public void IsNotNullOrEmpty_Empty()
        {
            var validator = new Validator<Foo>()
                .IsNotNullOrEmpty(x => x.StringValue);

            var model = new Foo { StringValue = string.Empty};
            var summary = validator.Validate(model);

            Assert.IsFalse(summary.IsValid);
            var res = summary.Results;

            Console.WriteLine("Errors: {0}", res.Count);
        }

        [Test]
        public void IsNotNullOrEmpty_NotNull()
        {
            var validator = new Validator<Foo>()
                .IsNotNullOrEmpty(x => x.StringValue);

            var model = new Foo { StringValue = "test" };
            var summary = validator.Validate(model);

            Assert.IsTrue(summary.IsValid);
            var res = summary.Results;

            Console.WriteLine("Errors: {0}", res.Count);
        }

        [Test]
        public void IsNotNullOrEmpty_Should_HaveSafeGet_When_TryingToGetNestedValue_And_ThereIsNullValueInChain()
        {
            var validator = new Validator<Foo>()
                .IsNotNullOrEmpty(x => x.Bar.StringValue);

            var summary = validator.Validate(new Foo());

            var res = summary.Results;
            Console.WriteLine("Errors: {0}", res.Count);

            Assert.IsFalse(summary.IsValid);
            
        }
    }
}