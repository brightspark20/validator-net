﻿using NUnit.Framework;

namespace BrightSpark.Validator.Test
{
    [TestFixture]
    public class InterfacedTypeValidator
    {
        public interface IFoo
        {
            string Name { get; set; }
            bool? IsTrue { get; set; }
        }

        public class Foo : IFoo
        {
            public string Name { get; set; }
            public bool? IsTrue { get; set; }
        }


        [Test]
        public void Should_ActAsNormalType()
        {
            var validator = new Validator<IFoo>()
                .Required(x => x.Name);

            var model = new Foo();

            var summary = validator.Validate(model);

            Assert.IsFalse(summary.IsValid);
        }


        [Test]
        public void Should_ActAsNormalType_When_UsingConditions()
        {
            var validator = new Validator<Foo>()
                .Required(x => x.Name)
                .When(x => x.IsTrue.HasValue, v => v
                    .IsTrue(y => y.IsTrue, y => y.IsTrue == true));

            var model = new Foo
            {
                Name = "test",
                IsTrue = true
            };

            var summary = validator.Validate(model);

            Assert.IsTrue(summary.IsValid);
        }


        [Test]
        public void Should_ActAsNormalType_When_UsingConditions_And_InvokedByConstraintedMethod()
        {
            var summary = Internal(new Foo());
            Assert.IsFalse(summary.IsValid);
        }

        private ValidationSummary Internal<T>(T model) where T : IFoo
        {
            var validator = new Validator<T>()
                .Required(x => x.Name)
                .When(x => x.IsTrue.HasValue, v => v
                    .IsTrue(y => y.IsTrue, y => y.IsTrue == true));

            return validator.Validate(model);
        }
    }
}