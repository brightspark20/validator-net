# Validator.NET



## Features

* Fluent
* Property validation
* Collection validation
* Conditional validation
* No dependencies

### Creating validator


```
#!c#

var validator = new Validator<Foo>()
    .IsNotNullOrEmpty(x => x.Name);
```

or dedicated class

```
#!c#

public class FooValidator : Validator<Foo>
{
    this.IsNotNullOrEmpty(x => x.Name);
}
```

### Validating properties

Properties can be validated through lambda expressions

```
#!c#

validator.Required(x => x.Name, "Name is required");
```

or via string key

```
#!c#

validator.Required("Name", "Name is required");
```

or via property validator

```
#!c#

.Property(x => x.IntValue).Is(p => p
  .GreaterThan(1, "Should be greater than {1}")
  .GreaterThanOrEqualTo(x => x.IntValue2, "Should be greater thant IntValue2({1})")
)
```

List of bundled validations:

**Comparable**

* IsGreaterThan
* IsGreaterThanOrEqualTo
* IsLessThan
* IsLessThanOrEqualTo
* IsEqualTo

**String**

* IsNotNullOrEmpty
* IsNull
* MinLength
* MaxLength
* IsEmail
* IsUri
* IsDate
* IsInteger
* IsNumeric
* Regex
* RegexOrNull

**Object** 

* Required  
  i.e value is not null  
  Unless rule is used on Enum value. Then validator succeeds when value is greater than zero.

### Sub-validators
```
#!c#

public class Foo
{
    public Bar Bar { get; set; }
}

public class Bar
{
    public string Id { get; set; }
    public string Name { get; set; }
}

```

```
#!c#

var barValidator = new Validator<Bar>()
    .Required(x => x.Id, "Id is required")
    .IsNumeric(x => x.Id, "Id must be numeric")
    .Required(x => x.Name, "Name is required");


var validator = new Validator<Foo>()
    .IsTrue(x => x.Bar, barValidator);

var validationSummary = validator.IsValid(new Foo());
```


### Validating collections
```
#!c#

public class Foo
{
    public Bar[] Bars { get; set; }
    public string[] Names { get; set; }
}

public class Bar
{
    public string Id { get; set; }
    public string Name { get; set; }
}

```

```
#!c#

var barValidator = new Validator<Bar>()
    .Required(x => x.Id, "Id is required")
    .IsNumeric(x => x.Id, "Id must be numeric")
    .Required(x => x.Name, "Name is required");

var nameValidator = new Validator<string>()
    .Required(x => x, "Name is required")

var validator = new Validator<Foo>()
    .ValidCollection(x => x.Names, nameValidator, "Invalid collection")
    .ValidCollection(x => x.Bars, barValidator, "Invalid collection");

var model = new Foo
{
    Bars = new[]
    {
        new Bar {Id = "1", Name = "test"},
        new Bar {Id = "asdf"},
        new Bar(),
    }
};

var summay = validator.Validate(model);
var isValid = summary.IsValid;
var res = summary.Results;
```

### Conditional validation


```
#!c#

public class Foo
{
    public bool HasStuff { get; set; }
    public string Stuff { get; set; }

    public bool HasOtherStuff { get; set; }
    public string OtherStuff { get; set; }
}
```


```
#!c#

var validator = new Validator<Foo>()
    .When(x => x.HasStuff, v => v
        .Required(x => x.Stuff, "Stuff required")
        .When(x => x.HasOtherStuff, v2 => v2
            .Required(x => x.OtherStuff, "Other stuff is also required")
        )
    );

```

or 


```
#!c#

IValidator<Foo> otherValidator = GetOtherValidator();

var validator = new Validator<Foo>()
    .When(x => x.HasStuff, v => v
        .Required(x => x.Stuff, "Stuff required")
        .When(x => x.HasOtherStuff, otherValidator)
    );

```