Framework "4.6.2"

properties { 
    
  $signAssemblies = $true
  $buildDocumentation = $false
  $buildNuGet = $true
  $treatWarningsAsErrors = $false
  
  $cwd = resolve-path .
  $toolsDir = "$cwd\Tools"

  $baseDir  = resolve-path ..
  $buildDir = "$baseDir\Build"
  $sourceDir = "$baseDir\Src"
  $docDir = "$baseDir\Doc"
  $releaseDir = "$baseDir\Release"
  $workingDir = "$baseDir\Working"
  
  $signKeyPath = "$sourceDir\BrightSpark.Validator\brightspark.pfx"
  
  $ns = "BrightSpark.Validator"
  
  $packageName = "Validator.NET"
  
  $builds = @(
   
	@{
		Root = "$ns\"; 
		Name = "$ns.csproj"; 
		TestsName = "$ns.Test"; 
		Constants="NET452;NETFULL"; 
		FinalDir="net452"; 
		NuGetDir = "net452"; 
		Framework="net452"; 
		Sign=$true
	},
	@{
		Root = "$ns\"; 
		Name = "$ns.csproj"; 
		TestsName = "$ns.Test"; 
		Constants="NET47;NETFULL"; 
		FinalDir="net47"; 
		NuGetDir = "net47"; 
		Framework="net47"; 
		Sign=$true
	},
	@{
		Root = "$ns\"; 
		Name = "$ns.csproj"; 
		TestsName = "$ns.Test"; 
		Constants="NETCORE;NETSTANDARD;NETSTANDARD2_0"; 
		FinalDir="netstandard2.0"; 
		NuGetDir = "netstandard2.0"; 
		Framework="netstandard2.0"; 
		Sign=$true
	}
  )
}


task default -depends Test



# Ensure a clean working directory
task Clean {
  Set-Location $baseDir
  
  if (Test-Path -path $workingDir)
  {
    Write-Output "Deleting Working Directory"
    
    del $workingDir -Recurse -Force
  }
  
  New-Item -Path $workingDir -ItemType Directory
}

# Build each solution, optionally signed
task Build -depends Clean { 
	
	msbuild /version
	
	foreach ($build in $builds)
	{
		$name = $build.Name
		$finalDir = $build.FinalDir
		$sign = ($build.Sign -and $signAssemblies)
		$targetFramework = $build.Framework;
		
		Write-Host -ForegroundColor Green "Building " $name
		Write-Host -ForegroundColor Green "Signed " $sign
		Write-Host
		
		exec { msbuild "/t:Clean;Rebuild" /p:TargetFramework=$targetFramework /p:Configuration=Release /p:DebugSymbols=true "/p:Platform=Any CPU" /p:OutputPath=bin\Release\$finalDir\ /p:AssemblyOriginatorKeyFile=$signKeyPath "/p:SignAssembly=$sign" "/p:TreatWarningsAsErrors=$treatWarningsAsErrors" (GetConstants $build.Constants $sign) ($sourceDir + "\" + $build.Root + $build.Name) | Out-Default } "Error building $name"
	}
}

# Optional build documentation, add files to final zip
task Package -depends Build {
  
  foreach ($build in $builds)
  {
    $name = $build.TestsName
	$root = $build.Root
    $finalDir = $build.FinalDir
    
	robocopy "$sourceDir\$root\bin\Release\$finalDir" $workingDir\Package\Bin\$finalDir * /NP | Out-Default
  }
  
  if ($buildNuGet)
  {
    New-Item -Path $workingDir\NuGet -ItemType Directory
    Copy-Item -Path "$buildDir\$packageName.nuspec" -Destination $workingDir\NuGet\$packageName.nuspec -recurse
    
    foreach ($build in $builds)
    {
      if ($build.NuGetDir -ne $null)
      {
        $name = $build.TestsName
		$root = $build.Root
        $finalDir = $build.FinalDir
        $frameworkDirs = $build.NuGetDir.Split(",")
        
        foreach ($frameworkDir in $frameworkDirs)
        {
          robocopy "$sourceDir\$root\bin\Release\$finalDir" $workingDir\NuGet\lib\$frameworkDir * /NP | Out-Default
        }
      }
    }
  
	$fw = $frameworkDirs[0];
	$currentVersion = GetVersion $workingDir\NuGet\lib\$fw\$ns.dll
  
    exec { .\build\Tools\NuGet\NuGet.exe pack $workingDir\NuGet\$packageName.nuspec -Symbols -version $currentVersion }
    move -Path .\*.nupkg -Destination $workingDir\NuGet
	
	$nupkg = "$workingDir\NuGet\$packageName.$currentVersion.symbols.nupkg"
	  Write-Host $nupkg
	
	$uploadNuGet = Read-Host 'Upload nuget? [y/n]'
	if ($uploadNuGet -eq "y")
	{

		if ($apiKey -eq $null) {
			$apiKey = Read-Host 'API key'
		}

		if ($nugetSource -eq $null)
		{
			exec { .\Build\Tools\NuGet\NuGet.exe setApiKey $apiKey }
			exec { .\Build\Tools\NuGet\nuget.exe push $nupkg -source https://api.nuget.org/v3/index.json }
		}
		else
		{
			exec { .\Build\Tools\NuGet\NuGet.exe setApiKey $apiKey -source $nugetSource }
			exec { .\Build\Tools\NuGet\nuget.exe push $nupkg -source $nugetSource }
		}
	}
  }
}

# Unzip package to a location
task Deploy -depends Package {
  echo deploy
}

# Run tests on deployed files
task Test -depends Deploy {
  echo test
}


function GetConstants($constants, $includeSigned)
{
  $signed = switch($includeSigned) { $true { ";SIGNED" } default { "" } }

  return "/p:DefineConstants=`"CODE_ANALYSIS;TRACE;$constants$signed`""
}

function GetVersion([string] $file)
{
	return [System.Diagnostics.FileVersionInfo]::GetVersionInfo($file).FileVersion
}
